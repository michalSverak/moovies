//
//  MoovieUITests.swift
//  MoovieUITests
//
//  Created by Michal Sverak on 9/15/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import XCTest

class MoovieUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSearchForMovie() {
        
        let app = XCUIApplication()
        XCUIApplication().navigationBars["Moovie.MoviesView"].children(matching: .button).element(boundBy: 1).tap()
        
        let searchForMovieOrActorSearchField = app.navigationBars["Moovie.SearchView"].searchFields["Search movie or actor..."]
        searchForMovieOrActorSearchField.tap()
        searchForMovieOrActorSearchField.typeText("Into the wild")
    }
}
