//
//  MovieDetailTop.swift
//  Moovie
//
//  Created by Michal Sverak on 8/4/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

@IBDesignable
class MovieDetailTop: UIView {
    
    @IBOutlet weak var movieImage: ImageViewWithFade!
    @IBOutlet weak var movieGenre: UILabel!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var movieDescription: UILabel!
    @IBOutlet weak var creators: UILabel!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var ratingRing: RatingRing!
    
    var contentainerView:UIView?
    @IBInspectable var nibName:String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        xibSetup()
    }
    
    func xibSetup() {
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        view.autoresizingMask =
            [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        contentainerView = view
    }
    
    func loadViewFromNib() -> UIView? {
        guard let nibName = nibName else { return nil }
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(
            withOwner: self,
            options: nil).first as? UIView
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        xibSetup()
        contentainerView?.prepareForInterfaceBuilder()
    }

    func setup(movieImage: UIImage, movieGenre: String, movieName: String, movieDescription: String, creators: [String], rating: CGFloat) {
    
        self.movieImage.image = movieImage
        self.movieGenre.text = movieGenre
        self.movieName.text = movieName
        self.movieDescription.text = movieDescription
        self.creators.text = setupCreators(creas: creators)
        ratingRing.backgroundColor = .clear
        ratingRing.setRating(percents: rating)
        contentView.backgroundColor = .clear
    }
    
    func setupCreators(creas: [String]) -> String {
        
        var crea = ""
        
        for i in 0..<creas.count {
            crea += creas[i] + ", "
        }
        
        return String(crea.characters.dropLast(2))
    }
}
