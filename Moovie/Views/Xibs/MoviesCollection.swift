//
//  MoviePosterCollection.swift
//  Moovie
//
//  Created by Michal Sverak on 8/3/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

protocol ShowDetailDelegate: class {
    
    func didSelectMovie(withId: Int, movie: FeaturedMovieStub)
}

class MoviesCollection: UIView, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBInspectable var nibName:String?
    
    var dataArray = [FeaturedMovieStub]()
    var contentView: UIView?
    weak var delegate: ShowDetailDelegate?
    let imageManager = ImageSizeManager.shared
    
    //MARK: Xib Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        xibSetup()
    }
    
    func xibSetup() {
        
        guard let view = loadViewFromNib() else {
            print("Cannot load view from nib: MoviesColletion")
            return
        }
    
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        contentView = view
        
        //register cell nib
        let bundle = Bundle(for: type(of: self))
        let cell = UINib(nibName: "MovieCollectionViewCell", bundle: bundle)
        collectionView.register(cell, forCellWithReuseIdentifier: "MovieCollectionViewCell")
    }
    
    func loadViewFromNib() -> UIView? {
        
        guard let nibName = nibName else {
            print("Cannot load view MoviesCollection from nib with given nibName")
            return nil
        }
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        
        return nib.instantiate(
            withOwner: self,
            options: nil).first as? UIView
    }
    
//    override func prepareForInterfaceBuilder() {
//        super.prepareForInterfaceBuilder()
//        
//        xibSetup()
//        contentView?.prepareForInterfaceBuilder()
//    }

    //MARK: Movies horizontal collection view
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        self.collectionView.backgroundView = nil
        
        if dataArray.count == 0 {
            
            // Set background view when there are no data

            let label = UILabel()
            label.center = self.collectionView.center
            label.font = UIFont(name: "Calibre", size: 16)
            label.text = "No movies here yet..."
            label.textAlignment = .center
            label.textColor = ColorPalette.inactiveButtonColor
            self.collectionView.backgroundView = label
        }
        
        return dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCollectionViewCell", for: indexPath) as? MovieCollectionViewCell else {
            
            return UICollectionViewCell()
        }
        
        cell.movieTitleLabel.isHidden = true

        if dataArray[indexPath.row].posterPath == "" {
            cell.movieTitleLabel.isHidden = false
            cell.movieTitleLabel.text = dataArray[indexPath.row].originalTitle
        }
        
        guard let basePath = imageManager.baseImageUrl(imagesize: Int(cell.moviePoster.frame.width), type: .poster) else {
            return cell
        }
        
        let imagePath = dataArray[indexPath.row].posterPath
        let imageUrl =  URL(string: basePath + imagePath)

        guard let url = imageUrl else {
            print("wrong image url in movie cell")
            return cell
        }
    
        cell.moviePoster.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "film-poster-placeholder"), filter: nil, progress: nil, progressQueue: .main, imageTransition: UIImageView.ImageTransition.crossDissolve(0.3), runImageTransitionIfCached: false, completion: nil)
    
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let movie = dataArray[indexPath.row]
        let id = dataArray[indexPath.row].movieId
        self.delegate?.didSelectMovie(withId: id, movie: movie)
    }
}
