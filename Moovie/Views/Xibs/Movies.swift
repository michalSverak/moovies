//
//  Movies.swift
//  Moovie
//
//  Created by Michal Sverak on 8/3/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

@IBDesignable
class Movies: UIView, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    var dataArray = [UIImage]()
    var contentView:UIView?
    @IBInspectable var nibName:String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        xibSetup()
    }
    
    func xibSetup() {
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        view.autoresizingMask =
            [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        contentView = view
        
        //register cell nib
        let bundle = Bundle(for: type(of: self))
        let cell = UINib(nibName: "MovieCollectionViewCell", bundle: bundle)
        collectionView.register(cell, forCellWithReuseIdentifier: "MovieCollectionViewCell")
    }
    
    func loadViewFromNib() -> UIView? {
        guard let nibName = nibName else { return nil }
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        
        return nib.instantiate(
            withOwner: self,
            options: nil).first as? UIView
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        xibSetup()
        contentView?.prepareForInterfaceBuilder()
    }

    //MARK: Movies horizontal collection view
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCollectionViewCell", for: indexPath) as? MovieCollectionViewCell {
            
            cell.moviePoster.image = dataArray[indexPath.row]
            return cell
            
        } else {
            return UICollectionViewCell()
        }
    }
}
