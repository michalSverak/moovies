//
//  GalleryCollectionViewCell.swift
//  Moovie
//
//  Created by Michal Sverak on 8/18/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var galleryItem: UIImageView!
}
