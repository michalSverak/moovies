//
//  ActorCollectionViewCell.swift
//  Moovie
//
//  Created by Michal Sverak on 8/4/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

class ActorCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var actorPhoto: UIImageView!
    @IBOutlet weak var actorName: UILabel!
    @IBOutlet weak var athorCharacter: UILabel!
}
