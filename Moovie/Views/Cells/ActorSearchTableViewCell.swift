//
//  ActorSearchTableViewCell.swift
//  Moovie
//
//  Created by Michal Sverak on 8/15/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

class ActorSearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var portraitImage: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var jobTitleLabel: UILabel!
}
