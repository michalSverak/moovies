//
//  MovieSearchTableViewCell.swift
//  Moovie
//
//  Created by Michal Sverak on 7/28/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

class MovieSearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var moviePoster: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieYearLabel: UILabel!
    @IBOutlet weak var ratingView: RatingRing!
}
