//
//  CommentTableViewCell.swift
//  Moovie
//
//  Created by Michal Sverak on 8/31/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var commentText: UILabel!
}
