//
//  ActorMoviesTableViewCell.swift
//  Moovie
//
//  Created by Michal Sverak on 8/18/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

class ActorMoviesTableViewCell: UITableViewCell {

    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var movieTitleLabel: UILabel!
}
