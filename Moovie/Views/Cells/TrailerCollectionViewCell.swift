//
//  TrailerCollectionViewCell.swift
//  Moovie
//
//  Created by Michal Sverak on 9/5/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

class TrailerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var trailerThumbnail: UIImageView!
}
