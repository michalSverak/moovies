//
//  MovieCollectionViewCell.swift
//  Moovie
//
//  Created by Michal Sverak on 8/3/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var moviePoster: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        if movieTitleLabel.text == nil {
            movieTitleLabel.isHidden = true
        }
    }
}
