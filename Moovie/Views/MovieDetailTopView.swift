//
//  MovieDetailTopView.swift
//  Moovie
//
//  Created by Michal Sverak on 8/4/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

class MovieDetailTopView: UIView {
    
    @IBOutlet weak var movieImage: ImageViewWithFade!
    @IBOutlet weak var movieGenre: UILabel!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var movieDescription: UILabel!
    @IBOutlet weak var creators: UILabel!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var ratingRing: RatingRing!
    @IBOutlet weak var creatorSectionLabel
    : UILabel!
    
    var contentainerView:UIView?
    @IBInspectable var nibName:String?
    
    //MARK: Xib Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        xibSetup()
        
        //Set initial state
        movieName.text = ""
        movieGenre.text = ""
        movieDescription.text = ""
        creators.text = ""
        ratingRing.backgroundColor = .clear
        contentView.backgroundColor = .clear
    }
    
    func xibSetup() {
        guard let view = loadViewFromNib() else {
            
            print("Cannot load view from nib: MovieDetailTopView")
            return
        }
        view.frame = bounds
        view.autoresizingMask =
            [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        contentainerView = view
    }
    
    func loadViewFromNib() -> UIView? {
        guard let nibName = nibName else {
            
            print("Cannot load view MovieDetailTopView from nib with given nibName")
            return nil
        }
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(
            withOwner: self,
            options: nil).first as? UIView
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        xibSetup()
        contentainerView?.prepareForInterfaceBuilder()
    }
    
    //MARK: View Setup

    func setup(movieGenre: String, movieName: String, movieDescription: String, creators: [String]) {
    
        self.movieGenre.text = movieGenre
        self.movieName.text = movieName
        self.movieDescription.text = movieDescription
        self.creators.text = setupCreators(creators: creators)
    }
    
    func setRating(rating: CGFloat) {
        
        ratingRing.isHidden = false
        
        if rating <= 0 {
            ratingRing.isHidden = true
        }
        
        ratingRing.setRating(percents: rating)
    }
    
    func setupCreators(creators: [String]) -> String {
        
        if creators.count > 1 {
            creatorSectionLabel.text = "CREATORS"
        }

        var formattedCreators = ""
        
        for i in 0..<creators.count {
            formattedCreators += creators[i] + ", "
        }
        
        return String(formattedCreators.characters.dropLast(2))
    }
}
