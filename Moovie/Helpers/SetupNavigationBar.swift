//
//  SetupNavigationBar.swift
//  Moovie
//
//  Created by Michal Sverak on 8/16/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

struct SetupNavigationBar {
    
    func setup() {
        
        //Setup navigation bar across whole app
        
        UINavigationBar.appearance().barTintColor = ColorPalette.backgroundColor
        UINavigationBar.appearance().tintColor = .white
        let navbarFont = UIFont(name: "Calibre-Semibold", size: 14) ?? UIFont.systemFont(ofSize: 14)
        UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: navbarFont, NSForegroundColorAttributeName:UIColor.white]
        
        let backImage = UIImage(named: "backArrow")?.withRenderingMode(.alwaysOriginal)
        UINavigationBar.appearance().backIndicatorImage = backImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -80.0), for: .default)
    }
}
