//
//  BorderForViews.swift
//  Moovie
//
//  Created by Michal Sverak on 7/27/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//
//Source: https://stackoverflow.com/questions/28854469/change-uibutton-bordercolor-in-storyboard

import UIKit

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}
