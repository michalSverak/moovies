//
//  StoryboardInit.swift
//  Moovie
//
//  Created by Michal Sverak on 8/7/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//
// Source: Jan Kaltoun @STRV

import UIKit

public protocol StoryboardInit { }

public extension StoryboardInit where Self: UIViewController {
    
    // MARK: - Storyboard Init
    
    static func storyboardInit() -> Self {
        
        return UIStoryboard(name: String(describing: self), bundle: Bundle(for: self)).instantiateInitialViewController() as! Self
    }
}
