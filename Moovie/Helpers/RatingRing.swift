//
//  RatingRing.swift
//  Moovie
//
//  Created by Michal Sverak on 7/28/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//
//Source before edit: https://makeapppie.com/2015/03/10/swift-swift-basic-core-graphics-for-the-ring-graph/

import UIKit

class RatingRing: UIView {
    
    var endArc:CGFloat = 0.0 {   // in range of 0.0 to 1.0
        didSet{
            setNeedsDisplay()
        }
    }
    
    let arcWidth:CGFloat = 5.0
    var arcColor = UIColor.clear
    var arcBackgroundColor = UIColor.clear
    var ratingLabel: UILabel?
    var rating = ""
    
    override func draw(_ rect: CGRect) {
        
        //Important constants for circle
        let fullCircle = 2.0 * CGFloat(Double.pi)
        let start:CGFloat = -0.25 * fullCircle
        let end:CGFloat = endArc * fullCircle + start
        
        //find the centerpoint of the rect
         let centerPoint = CGPoint(x: rect.midX, y: rect.midY)
        
        //define the radius by the smallest side of the view
        var radius:CGFloat = 0.0
        if rect.width > rect.width{
            radius = (rect.width - arcWidth) / 2.0
        }else{
            radius = (rect.width - arcWidth) / 2.0
        }
        
        //starting point for all drawing code is getting the context.
        let ctx = UIGraphicsGetCurrentContext()
        
        //set colorspace
        _ = CGColorSpaceCreateDeviceRGB()
        
        //set line attributes
        
        guard let context = ctx else {
            
            print("UIGraphicsGetCurrentContext was nil while drawing RatingRing")
            return
        }
        
        context.setLineWidth(arcWidth)
        context.setLineCap(CGLineCap.round)
        
        //make the background circle
        context.setStrokeColor(arcBackgroundColor.cgColor)
        context.addArc(center: centerPoint, radius: radius, startAngle: 0, endAngle: fullCircle, clockwise: true)
        context.strokePath()
        context.setStrokeColor(arcColor.cgColor)
        context.addArc(center: centerPoint, radius: radius, startAngle: start, endAngle: end, clockwise: false)
        context.strokePath()
        
        //Add label to the middle
        
        if ratingLabel == nil {
            ratingLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 20))
        }
        
        guard let label = ratingLabel else {
            return
        }
        
        label.center = centerPoint
        label.textAlignment = .center
        label.text = rating
        label.textColor = arcColor
        label.font = UIFont(name: "Calibre", size: 16)
        self.addSubview(label)
    }
}

extension RatingRing {
    
    func setRating(percents: CGFloat) {
        
        self.endArc = percents
        
        var ringColor = UIColor.clear
        var backgroundColor = UIColor.clear
        
        switch percents {
        case 0...0.5 :
            ringColor = ColorPalette.yellowRingColor
            backgroundColor = ColorPalette.yellowRingBackgroundColor
        case 0.5...1 :
            ringColor = ColorPalette.greenRingColor
            backgroundColor = ColorPalette.greenRingBackgroundColor
        default:
            break
        }
        
        self.arcColor = ringColor
        self.arcBackgroundColor = backgroundColor
        
        //Setup rating label
        
        self.rating = "\(Int(percents*100))%"
    }
}
