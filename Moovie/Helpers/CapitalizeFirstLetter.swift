//
//  CapitalizeFirstLetter.swift
//  Moovie
//
//  Created by Michal Sverak on 9/19/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation

extension String {
    func capitalizingFirstLetter() -> String {
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
