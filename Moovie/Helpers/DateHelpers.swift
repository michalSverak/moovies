//
//  DateHelper.swift
//  Moovie
//
//  Created by Michal Sverak on 8/14/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation

extension Date {
    
    private static let dateFormatter = DateFormatter()
    
    // Returns week day name
    func dateToString() -> String? {
        
        Date.dateFormatter.dateFormat = "yyyy-MM-dd"
        return Date.dateFormatter.string(from: self)
    }
    
    func yearToString() -> String? {
        
        Date.dateFormatter.dateFormat = "YYYY"
        return Date.dateFormatter.string(from: self)
    }
}
