//
//  ImageViewWithFade.swift
//  Moovie
//
//  Created by Michal Sverak on 7/30/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

//MARK: Fade effect for UIImageView

class ImageViewWithFade: UIImageView {
    
    let gradient: CAGradientLayer = CAGradientLayer()
    let backgroundCol = UIColor(colorLiteralRed: 19/255.0, green: 20/255.0, blue: 23/255.0, alpha: 1.0)
    
    override func awakeFromNib() {
        
        self.image = self.image?.alpha(0.67)
        gradient.frame = self.bounds
        gradient.colors = [UIColor.clear.cgColor, backgroundCol.cgColor]
        gradient.locations = [0.5, 1.0]
        self.layer.insertSublayer(gradient, at: 0)
    }
}

//MARK: Alpha setting for UIImage

extension UIImage {
    
    func alpha(_ value:CGFloat) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(at: CGPoint.zero, blendMode: .normal, alpha: value)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}

//MARK: Blue effetc for UIImageView

extension UIImageView {
    
    func blurImage() {
        
        guard let image = self.image else {
            return
        }
        
        let context = CIContext(options: nil)
        let inputImage = CIImage(image: image)
        let originalOrientation = image.imageOrientation
        let originalScale = image.scale
        
        let filter = CIFilter(name: "CIGaussianBlur")
        filter?.setValue(inputImage, forKey: kCIInputImageKey)
        filter?.setValue(1.0, forKey: kCIInputRadiusKey)
        let outputImage = filter?.outputImage
        
        var cgImage:CGImage?
        
        if let asd = outputImage {
            cgImage = context.createCGImage(asd, from: (inputImage?.extent)!)
        }
        
        if let cgImageA = cgImage {
            self.image = UIImage(cgImage: cgImageA, scale: originalScale, orientation: originalOrientation)
        }
    }
}

//MARK: UIIMage screenshot extension

extension UIImage {
    
    class func takeScreenshot(view: UIView) -> UIImage? {

        UIGraphicsBeginImageContext(view.bounds.size)
        
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let screenshot:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        UIGraphicsEndImageContext()
        return screenshot
    }
}
