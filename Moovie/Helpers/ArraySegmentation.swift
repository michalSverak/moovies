//
//  ArrayIntoSegments.swift
//  Moovie
//
//  Created by Michal Sverak on 8/29/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation

extension Array {
    func segments(_ segmentSize: Int) -> [[Element]] {
        return stride(from: 0, to: self.count, by: segmentSize).map {
            Array(self[$0..<Swift.min($0 + segmentSize, self.count)])
        }
    }
}
