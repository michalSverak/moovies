//
//  ColorPalette.swift
//  Moovie
//
//  Created by Michal Sverak on 8/16/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

struct ColorPalette {
    
    //Black background color of UI views
    static let backgroundColor = UIColor(colorLiteralRed: 19/255.0, green: 20/255.0, blue: 22/255.0, alpha: 1.0)
    
    //Grey color of inactive button
    static let inactiveButtonColor = UIColor(colorLiteralRed: 90/255.0, green: 90/255.0, blue: 92/255.0, alpha: 1.0)
    
    //Rating stars
    
    static let starRed = UIColor(colorLiteralRed: 252/255.0, green: 0, blue: 0, alpha: 1.0)
    static let starGrey = UIColor(colorLiteralRed: 43/255.0, green: 43/255.0, blue: 45/255.0, alpha: 1.0)
    
    //MARK: Action sheet
    
    static let buttonRed = UIColor(colorLiteralRed: 252/255.0, green: 0, blue: 0, alpha: 1.0)
    static let buttonGrey = UIColor(colorLiteralRed: 19/255.0, green: 20/255.0, blue: 22/255.0, alpha: 1.0)
    
    //Rating 0.5...1.0
    
    static let greenRingColor = UIColor(colorLiteralRed: 0, green: 210/255.0, blue: 119/255.0, alpha: 1.0)
    
    //Ring Green Background color
    
    static let greenRingBackgroundColor = UIColor(colorLiteralRed: 0, green: 210/255.0, blue: 119/255.0, alpha: 0.1)
    
    //Rating 0...0.5
    
    static let yellowRingColor = UIColor(colorLiteralRed: 210/255.0, green: 215/255.0, blue: 16/255.0, alpha: 1.0)
    
    //Ring Yellow Background color
    
    static let yellowRingBackgroundColor = UIColor(colorLiteralRed: 210/255.0, green: 215/255.0, blue: 16/255.0, alpha: 0.1)
    
    //Actor Movies
    
    static let characterGrey = UIColor(colorLiteralRed: 137/255.0, green: 137/255.0, blue: 138/255.0, alpha: 1.0)
    
    //Global tint
    
    static let STRVred = UIColor(red:0.82, green:0.00, blue:0.00, alpha:1.00)
    
    //Search bar background
    
    static let searchBarGreyBackground = UIColor(colorLiteralRed: 34/255.0, green: 34/255.0, blue: 37/255.0, alpha: 1.0)
    
    //Separator color
    
    static let separatorColor = UIColor(colorLiteralRed: 1.0, green: 1.0, blue: 1.0, alpha: 0.1)
    
    //Unselected genre
    
    static let unselectedGenre = UIColor(colorLiteralRed: 137/255.0, green: 137/255.0, blue: 138/255.0, alpha: 1.0)
}
