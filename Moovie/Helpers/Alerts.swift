//
//  Alerts.swift
//  Moovie
//
//  Created by Michal Sverak on 9/12/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation
import UIKit

protocol UIViewControllerProtocol {
    func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Swift.Void)?)
}

extension UIViewController: UIViewControllerProtocol { }

protocol AlertPresenter: UIViewControllerProtocol { }

extension AlertPresenter where Self: UIViewControllerProtocol {
    
    func showAlert(title: String, message: String, actionTitle: String, ButtonAction: (() -> Swift.Void)?) {
        
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: actionTitle, style: .default, handler: { _ in
            
            guard let action = ButtonAction else {
                return
            }
            
            action()
            
        }))
        
        if ButtonAction != nil {
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        }
        
        self.present(alert, animated: true, completion: nil)
    }
}
