//
//  AppSettings.swift
//  Moovie
//
//  Created by Michal Sverak on 8/8/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

struct AppSettings {
    
    // Store strings and values here as much as possible to not have them floating arounf the app
    
    static let apiKey = "c361d02c5dfff7d63adfafbb344088f9"
    static let apiLanguage = "en-US"
    static let apiImageLanguage = "en,null"
    static let apiRegion = "US"
    static let baseUrl = "https://api.themoviedb.org/3"
    static let actorMoviesDevidedIntoSegments = 6
    static let tabBarHeight: CGFloat = 57.0
    static let authenticationUrl = "https://www.themoviedb.org/authenticate/"
    static let sessionIDKey = "sessionIDKey"
    static let gravatarUrl = "https://www.gravatar.com/avatar/"
    static let includeAdultContent = false
    static let progressHUDDelay = 1.0
    static func youtubeThumbnailImagePath(key: String) -> String {
        return "https://img.youtube.com/vi/\(key)/0.jpg"
    }
}
