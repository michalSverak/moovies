//
//  MovieEntities.swift
//  Moovie
//
//  Created by Michal Sverak on 8/21/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation
import Unbox

struct MovieGenre: Unboxable {
    let id: Int
    let name: String
    
    init(unboxer: Unboxer) throws {
        id = try unboxer.unbox(key: "id")
        name = try unboxer.unbox(key: "name")
    }
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
}

struct APIMovieResults: Unboxable {
    let results: [FeaturedMovie]
    
    init(unboxer: Unboxer) throws {
        results = try unboxer.unbox(key: "results")
    }
}

struct FeaturedMovie: Unboxable {
    
    let movieId: Int
    let genreIds: [Int]?
    let posterPath: String?
    let originalTitle: String
    let releaseDate: Date?
    let imagePath: String?
    let voteAverage: Double?
    let overview: String?
    let character: String?
    
    init(unboxer: Unboxer) throws {
        
        movieId = try unboxer.unbox(key: "id")
        posterPath = unboxer.unbox(key: "poster_path")
        genreIds = unboxer.unbox(key: "genre_ids")
        originalTitle = try unboxer.unbox(key: "original_title")
        let df = DateFormatter()
        df.dateFormat = "YYYY-MM-dd"
        releaseDate = unboxer.unbox(key: "release_date", formatter: df)
        imagePath = unboxer.unbox(key: "backdrop_path")
        voteAverage = unboxer.unbox(key: "vote_average")
        overview = unboxer.unbox(key: "overview")
        character = unboxer.unbox(key: "character")
    }
}

struct FeaturedMovieStub: Item {
    
    var movieId: Int
    var genres: [String]
    var posterPath: String
    var originalTitle: String
    var releaseDate: String?
    var imagePath: String
    var overview: String
    var rating: Double
    var character: String
 
}

struct Movie: Unboxable {
    
    var movieId: Int
    var voteAverage: Double? = nil
    var genreIds: [Int]? = nil
    var originalTitle: String
    var imagePath: String? = nil
    var adult: Bool
    var posterPath: String? = nil
    var overview: String
    var releaseDate: String
    var hasVideo: Bool
    var actors: [MovieActor]
    var crew: [MovieCrew]
    var comments: [MovieComment]? = nil
    var images: [MovieImage]? = nil
    var videos: [MovieVideo]? = nil
    
    init(movieId: Int, originalTitle: String, adult: Bool, overview: String, releaseDate: String, hasVideo: Bool, actors: [MovieActor], crew: [MovieCrew]) {
        
        self.movieId = movieId
        self.originalTitle = originalTitle
        self.adult = adult
        self.overview = overview
        self.releaseDate = releaseDate
        self.hasVideo = hasVideo
        self.actors = actors
        self.crew = crew
    }

    init(unboxer: Unboxer) throws {
        
        movieId = try unboxer.unbox(key: "id")
        voteAverage = unboxer.unbox(key: "vote_average")
        genreIds = unboxer.unbox(key: "genre_ids")
        originalTitle = try unboxer.unbox(key: "original_title")
        imagePath = unboxer.unbox(key: "backdrop_path")
        adult = try unboxer.unbox(key: "adult")
        posterPath = unboxer.unbox(key: "poster_path")
        overview = try unboxer.unbox(key: "overview")
        releaseDate = try unboxer.unbox(key: "release_date")
        hasVideo = try unboxer.unbox(key: "video")
        actors = try unboxer.unbox(keyPath: "credits.cast")
        crew = try unboxer.unbox(keyPath: "credits.crew")
        comments = unboxer.unbox(keyPath:"reviews.results")
        images = unboxer.unbox(keyPath:"images.backdrops")
        videos = unboxer.unbox(keyPath:"videos.results")
    }
}

extension Movie: Equatable {
    
    public static func ==(lhs: Movie, rhs: Movie) -> Bool {
        return lhs.movieId == rhs.movieId &&
            lhs.originalTitle == rhs.originalTitle &&
            lhs.overview == rhs.overview
    }
}

struct MovieComment: Unboxable {
    
    let id: String
    let author: String
    let content: String?
    
    init(unboxer: Unboxer) throws {
        
        id = try unboxer.unbox(key: "id")
        author  = try unboxer.unbox(key: "author")
        content = unboxer.unbox(key: "content")
    }
}

struct PostedMovieStatus: Unboxable {
    
    /*
     status codes:
     1 success
     12 item updated succesfully
     13 item removed succesfully
     */
    
    let statusCode: Int?
    
    init(unboxer: Unboxer) throws {
        statusCode = unboxer.unbox(keyPath:"status_code")
    }
}
