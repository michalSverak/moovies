//
//  UserEntity.swift
//  Moovie
//
//  Created by Michal Sverak on 9/8/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation
import Unbox

struct User: Unboxable {
    
    let avatarUrl: String?
    let id: Int
    let name: String?
    let username: String?
    
    init(unboxer: Unboxer) throws {
        
        avatarUrl = unboxer.unbox(keyPath: "avatar.gravatar.hash")
        id = try unboxer.unbox(key:"id")
        name =  unboxer.unbox(key: "name")
        username = unboxer.unbox(key: "username")
    }
}
