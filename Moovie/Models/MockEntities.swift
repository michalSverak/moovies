//
//  MockEntities.swift
//  Moovie
//
//  Created by Michal Sverak on 8/4/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

protocol Item {}

class MockActor: Item {
    
    var name: String?
    var characters: String?
    var image: UIImage?
    
    init(name: String, characters: String?, image: UIImage) {
        
        self.name = name
        self.characters = characters
        self.image = image
    }
}

class MockMovie: Item {
    
    var title: String?
    var rating: Double?
    var image: UIImage?
    var year: Int?
    
    init(title: String, rating: Double, image: UIImage, year: Int) {
        
        self.title = title
        self.rating = rating
        self.image = image
        self.year = year
    }
}
