//
//  MediaEntities.swift
//  Moovie
//
//  Created by Michal Sverak on 8/21/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation
import Unbox

struct ImageSizes: Unboxable {
    
    let backdropSizes: [String]
    let baseUrl: String
    let logoSizes: [String]
    let posterSizes: [String]
    let profileSizes: [String]
    let secureBaseUrl: String
    let stillSizes: [String]
    
    init(unboxer: Unboxer) throws {
        
        backdropSizes = try unboxer.unbox(key: "backdrop_sizes")
        baseUrl = try unboxer.unbox(key: "base_url")
        logoSizes = try unboxer.unbox(key: "logo_sizes")
        posterSizes = try unboxer.unbox(key: "poster_sizes")
        profileSizes = try unboxer.unbox(key: "profile_sizes")
        secureBaseUrl = try unboxer.unbox(key: "secure_base_url")
        stillSizes = try unboxer.unbox(key: "still_sizes")
    }
}

struct MovieImage: Unboxable {
    
    let imagePath: String
    
    init(unboxer: Unboxer) throws {
        
        imagePath =  try unboxer.unbox(keyPath: "file_path")
    }
}

struct MovieVideo: Unboxable {
    
    let key: String
    
    init(unboxer: Unboxer) throws {
        
        key = try unboxer.unbox(key: "key")
    }
}
