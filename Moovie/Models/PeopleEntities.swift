//
//  PeopleEntities.swift
//  Moovie
//
//  Created by Michal Sverak on 8/21/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation
import Unbox

struct MovieTitle: Unboxable {
    
    let title: String?
    
    init(unboxer: Unboxer) throws {
        title = unboxer.unbox(key: "original_title")
    }
}

struct PopularActor: Unboxable, Item {
    
    let actorId: Int
    let name: String
    let knownFor: [MovieTitle]
    let imagePath: String?
    
    init(unboxer: Unboxer) throws {
        actorId = try unboxer.unbox(key: "id")
        name = try unboxer.unbox(key: "name")
        imagePath = unboxer.unbox(key: "profile_path")
        knownFor = try unboxer.unbox(key: "known_for")
    }
}

struct MovieActor: Unboxable {
    
    let actorId: Int
    let name: String
    let character: String
    let imagePath: String?
    
    init(unboxer: Unboxer) throws {
        
        actorId = try unboxer.unbox(key: "id")
        name = try unboxer.unbox(key: "name")
        character = try unboxer.unbox(key: "character")
        imagePath = unboxer.unbox(key: "profile_path")
    }
}

struct ActorStub {
    
    let actorId: Int
    let gender: String
    let name: String
    let bio: String
    let birthday: String
    let placeOfBirth: String
    let imagePath: String?
    let movies: [FeaturedMovieStub]
}

struct Actor: Unboxable {
    
    // Gender 1 = female, 2 = male
    
    let actorId: Int
    let gender: Int
    let name: String
    let bio: String
    let birthday: Date?
    let placeOfBirth: String?
    let imagePath: String?
    let movies: [FeaturedMovie]
    
    init(unboxer: Unboxer) throws {
        actorId = try unboxer.unbox(key: "id")
        gender = try unboxer.unbox(key: "gender")
        name = try unboxer.unbox(key: "name")
        bio = try unboxer.unbox(key: "biography")
        let df = DateFormatter()
        df.dateFormat = "YYYY-MM-dd"
        birthday = unboxer.unbox(key: "birthday", formatter: df)
        placeOfBirth = unboxer.unbox(key: "place_of_birth")
        imagePath = unboxer.unbox(key: "profile_path")
        movies = try unboxer.unbox(keyPath: "credits.cast")
    }
}

struct APIActorResults: Unboxable {
    let results: [PopularActor]
    
    init(unboxer: Unboxer) throws {
        results = try unboxer.unbox(key: "results")
    }
}

struct MovieCrew: Unboxable {
    
    let department: String
    let job: String
    let name: String
    
    init(unboxer: Unboxer) throws {
        
        department = try unboxer.unbox(keyPath: "department")
        job = try unboxer.unbox(keyPath: "job")
        name = try unboxer.unbox(keyPath: "name")
    }
}
