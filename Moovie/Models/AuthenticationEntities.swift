//
//  AuthenticationEntities.swift
//  Moovie
//
//  Created by Michal Sverak on 9/7/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation
import Unbox

struct Token: Unboxable {
    
    let token: String
    
    init(unboxer: Unboxer) throws {
        
        token = try unboxer.unbox(key: "request_token")
    }
}

struct SessionID: Unboxable {
    
    let sessionID: String
    
    init(unboxer: Unboxer) throws {
        
        sessionID = try unboxer.unbox(key: "session_id")
    }
}
