//
//  SearchViewController.swift
//  Moovie
//
//  Created by Michal Sverak on 7/28/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit
import SVProgressHUD

enum SearchTypes {
    case recentSearches
    case movies
    case actors
}

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, StoryboardInit, UISearchBarDelegate, SearchViewModelDelegate, AlertPresenter {
    
    //MARK: Outlets

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var moviesButton: UIButton!
    @IBOutlet weak var actorsButton: UIButton!
    @IBOutlet weak var recentSearchesLabel: UILabel!
    @IBOutlet weak var noResultsLabel: UILabel!
    
    //MARK: Variables and constants
    
    var querryString = ""
    let searchBar = UISearchBar()
    var dataArray = [Item]() {
        didSet {
             tableView.reloadData()
        }
    }
    var currentSearchType: SearchTypes?
    var firstSearchType: SearchTypes?
    var viewModel: SearchViewModel!
    let imageManager = ImageSizeManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //ViewModel initialization
        
        viewModel = SearchViewModel.resolve()
        viewModel.delegate = self
        
        //Initial View setup
        
        searchBarSetup()
        setRecentSearchesSearchType()
        tableView.delegate = self
        navBarSetup()
    }
    
    //MARK: ViewModel delegate methods
    
    func updateView() {
        
         SVProgressHUD.dismiss(withDelay: AppSettings.progressHUDDelay)
        
        guard let searchType = self.currentSearchType else {
            return
        }
        
        switch searchType {
        case .movies :
             dataArray = viewModel.movies
        case .actors :
            dataArray = viewModel.actors
        case .recentSearches:
            
            if firstSearchType == .movies {
                dataArray = Array(viewModel.movies.prefix(5))
                return
            }

            dataArray = Array(viewModel.actors.prefix(5))
        }
        
        if viewModel.movies.isEmpty {
            
            self.noResultsLabel.isHidden = false
            self.noResultsLabel.text = "No search results for \(querryString)"
        }
    }
    
    //Error handling
    
    func handleError(_ error: Error) {
        
        self.showAlert(title: error.localizedDescription, message: error.localizedDescription, actionTitle: "Try again") {
            
            guard let searchType = self.currentSearchType else {
                return
            }
            
            switch searchType {
            case .actors :
                self.viewModel.searchForActors(queryString: self.querryString)
            case .movies:
                self.viewModel.searchForMovies(queryString: self.querryString)
            default:
                break
            }
        }
        
        self.showAlert(title: error.localizedDescription, message: error.localizedDescription, actionTitle: "OK", ButtonAction: nil)
    }
    
    //MARK: Search methods
    
    func saveQuerryString() {
        
        guard let searchType = self.currentSearchType else {
            return
        }
        
        guard !self.querryString.isEmpty else {
            return
        }

        switch searchType {
        case .actors :
            self.viewModel.userDefaultsStorage.lastSearchedActor = self.querryString
        case .movies:
            self.viewModel.userDefaultsStorage.lastSearchedMovie = self.querryString
        default:
            break
        }
    }
    
    func setInitialSearchType() {
        guard let searchType = self.firstSearchType else {
            return
        }
        
        switch searchType {
        case .actors :
            setActorSearchType()
            firstSearchType = nil
        case .movies:
            setMovieSearchType()
            firstSearchType = nil
        default:
            break
        }
    }
    
    func setMovieSearchType() {
        
        moviesButton.isHidden = false
        actorsButton.isHidden = false
        recentSearchesLabel.isHidden = true
        self.currentSearchType = .movies
        self.dataArray = viewModel.movies
        moviesButton.setTitleColor(.white, for: .normal)
        actorsButton.setTitleColor(ColorPalette.inactiveButtonColor, for: .normal)
    }
    
    func setActorSearchType() {
        
        moviesButton.isHidden = false
        actorsButton.isHidden = false
        recentSearchesLabel.isHidden = true
        self.currentSearchType = .actors
        self.dataArray = viewModel.actors
        actorsButton.setTitleColor(.white, for: .normal)
        moviesButton.setTitleColor(ColorPalette.inactiveButtonColor, for: .normal)
    }
    
    func setRecentSearchesSearchType() {
        
        self.currentSearchType = .recentSearches
        moviesButton.isHidden = true
        actorsButton.isHidden = true
        recentSearchesLabel.isHidden = false
        
        guard let searchType = self.firstSearchType else {
            return
        }
        
        switch searchType {
            
        case .movies:
            if let qString = viewModel.userDefaultsStorage.lastSearchedMovie {
                SVProgressHUD.show()
                viewModel.searchForMovies(queryString: qString)
            }
        case .actors:
            if let qString = viewModel.userDefaultsStorage.lastSearchedActor {
                SVProgressHUD.show()
                viewModel.searchForActors(queryString: qString)
            }
        default:
            break
        }
    }
    
    @IBAction func setSearchType(_ sender: Any) {
        
        guard let searchType = self.currentSearchType else {
            return
        }
        
        self.dataArray = [Item]()
        self.searchBar.text = nil
        
        switch searchType {
        case .actors :
            setMovieSearchType()
        case .movies:
            setActorSearchType()
        default:
            break
        }
    }
    
    //MARK: View setup methods
    
    func navBarSetup() {
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        let buttonCancel = UIBarButtonItem(title: "CANCEL", style: .plain, target: self, action: #selector(SearchViewController.cancel))
        
        let barbuttonFont = UIFont(name: "Calibre-Medium", size: 13) ?? UIFont.systemFont(ofSize: 13)
        buttonCancel.setTitleTextAttributes([NSFontAttributeName: barbuttonFont, NSForegroundColorAttributeName: UIColor.white], for: .normal)
        
        self.navigationItem.rightBarButtonItem = buttonCancel
    }
    
    func searchBarSetup() {
        
        searchBar.showsCancelButton = false
        searchBar.delegate = self
        searchBar.keyboardAppearance = .dark
        
        let searchField = searchBar.value(forKey: "searchField") as? UITextField
        searchBar.tintColor = UIColor.white
        searchBar.barStyle = .black
        searchBar.placeholder = "Search movie or actor..."
        
        if let searchField = searchField {
            searchField.backgroundColor = ColorPalette.searchBarGreyBackground
            searchField.textColor = UIColor.white
        }
        
        searchBar.setSearchFieldBackgroundImage(#imageLiteral(resourceName: "bar"), for: .normal)
        searchBar.layer.cornerRadius = 2
        searchBar.layer.masksToBounds = true
        
        self.navigationItem.titleView = searchBar
    }
    
    //MARK: Transitions
    
    func cancel() {
        
        saveQuerryString()
        self.dismiss(animated: true, completion: nil)
    }
}

extension SearchViewController {
    
    //MARK: Search bar delegate
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        setInitialSearchType()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard let querryString = searchBar.text else {
            return
        }
        
        guard searchBar.text?.isEmpty == false else {
            return
        }
        
        guard let searchType = self.currentSearchType else {
            return
        }
        
        self.querryString = querryString
        
        switch searchType {
        case .actors :
            viewModel.searchForActors(queryString: querryString)
        case .movies:
            viewModel.searchForMovies(queryString: querryString)
        default:
            break
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
    }
}

extension SearchViewController {
    
    //MARK: Table view delegate methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        self.noResultsLabel.isHidden = true
        
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let movie = dataArray[indexPath.row] as? FeaturedMovieStub {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MovieSearchTableViewCell", for: indexPath) as? MovieSearchTableViewCell else {
                
                return UITableViewCell()
            }
            
            cell.ratingView.isHidden = false
            cell.movieTitleLabel.text = movie.originalTitle
            cell.movieYearLabel.text = movie.releaseDate
            cell.ratingView.setRating(percents: CGFloat(movie.rating / 10.0))
            
            if movie.rating == 0 {
                cell.ratingView.isHidden = true
            }
            
            guard let basePath = imageManager.baseImageUrl(imagesize: Int(cell.moviePoster.frame.width), type: .backdrop) else {
                return cell
            }
            
            let imageUrl =  URL(string: basePath + movie.posterPath)
            
            guard let url = imageUrl else {
                print("wrong image url in movie cell")
                return cell
            }
            
            cell.moviePoster.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "film-poster-placeholder"), filter: nil, progress: nil, progressQueue: .main, imageTransition: UIImageView.ImageTransition.crossDissolve(0.3), runImageTransitionIfCached: false, completion: nil)
            
            return cell

        }
            
        else if let actor = dataArray[indexPath.row] as? PopularActor {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ActorSearchTableViewCell", for: indexPath) as? ActorSearchTableViewCell else {
                
                return UITableViewCell()
            }
            
            cell.fullNameLabel.text = actor.name
            cell.jobTitleLabel.text = "Actor"
            cell.portraitImage.image = #imageLiteral(resourceName: "ActorPlaceholder")
            
            guard let imgPath = actor.imagePath else {
                return cell
            }
            
            guard let basePath = imageManager.baseImageUrl(imagesize: Int(cell.portraitImage.frame.width), type: .backdrop) else {
                return cell
            }
            
            let imageUrl =  URL(string: basePath + imgPath)
            
            guard let url = imageUrl else {
                print("wrong image url in actor cell")
                return cell
            }
            
            cell.portraitImage.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "ActorPlaceholder"), filter: nil, progress: nil, progressQueue: .main, imageTransition: UIImageView.ImageTransition.crossDissolve(0.3), runImageTransitionIfCached: false, completion: nil)
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if let movie = dataArray[indexPath.row] as? FeaturedMovieStub {
            
            let vc = MovieDetailViewController.storyboardInit()
            vc.currentMovieId = movie.movieId
            vc.movieStub = movie
            self.navigationController?.pushViewController(vc, animated: true)
          
        }  else if let actor = dataArray[indexPath.row] as? PopularActor {
            
            let vc = ActorDetailViewController.storyboardInit()
            vc.actorId = String(actor.actorId)
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
