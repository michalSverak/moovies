//
//  ActorMoviesPagingController.swift
//  Moovie
//
//  Created by Michal Sverak on 8/18/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

protocol ActorMoviesPagesDelegate: class {
    func currentPage(page: Int)
}

class ActorMoviesPagingController: UIPageViewController {
    
    //MARK: Variables and constants
    
    var orderedViewControllers = [UIViewController]()
    var movies = [FeaturedMovieStub]()
    weak var currentPageDelegate: ActorMoviesPagesDelegate?
    var numberOfPages = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setup view

        dataSource = self
        
        divideDataToViewControllers()
        
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
    }
    
    func divideDataToViewControllers() {
            
        let segmentedArray = movies.segments(AppSettings.actorMoviesDevidedIntoSegments)
        numberOfPages = segmentedArray.count
        
        for item in segmentedArray {
            
            let vc = ActorMoviesViewController.storyboardInit()
            orderedViewControllers.append(vc)
            vc.dataSource = item
        }
    }
}

extension ActorMoviesPagingController: UIPageViewControllerDataSource {
    
    //MARK: UIPageViewControllerDataSource
    
    // Page Before
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        currentPageDelegate?.currentPage(page: viewControllerIndex)
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    //Page After
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        currentPageDelegate?.currentPage(page: viewControllerIndex)
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
}
