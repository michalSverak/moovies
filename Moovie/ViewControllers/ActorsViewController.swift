//
//  ActorsViewController.swift
//  Moovie
//
//  Created by Michal Sverak on 8/8/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit
import SVProgressHUD

class ActorsViewController: UIViewController, StoryboardInit, UICollectionViewDelegate, UICollectionViewDataSource, ActorsViewModelDelegate, UICollectionViewDelegateFlowLayout, AlertPresenter {
    
    //MARK: Outlets
    
    @IBOutlet weak var actorsCollectionView: UICollectionView!
    
    //MARK: Variables and constants
    
    var actorsArray = [PopularActor]()
    var viewModel: ActorsViewModel!
    let imageManager = ImageSizeManager.shared

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Initialize viewModel
        viewModel = ActorsViewModel.resolve()
        viewModel.delegate = self
        
        //Initial data load
        dataReload()
        
        //Register collection view cells
        let cell = UINib(nibName: "ActorCollectionViewCell", bundle: nil)
        actorsCollectionView.register(cell, forCellWithReuseIdentifier: "ActorCollectionViewCell")
        
        //Setup view
        navBarSetup()
    }
    
    //MARK: Data reload
    
    func dataReload() {
        
        SVProgressHUD.show()
        viewModel.getMostPopularActors()
    }
    
    //MARK: ActorsViewModelDelegate methods
    
    func dataDownloaded() {
        
        SVProgressHUD.dismiss(withDelay: AppSettings.progressHUDDelay)

        actorsArray = viewModel.popularActors
        actorsCollectionView.reloadData()
    }
    
    //Error handling
    
    func handleError(_ error: Error) {
        
        SVProgressHUD.dismiss(withDelay: AppSettings.progressHUDDelay)
        
        self.showAlert(title: error.localizedDescription, message: error.localizedDescription, actionTitle: "Try Again") {
            self.dataReload()
        }
    }
    
    //MARK: View setup methods
    
    func navBarSetup() {
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true

        let buttonSearch = UIBarButtonItem(image: #imageLiteral(resourceName: "Search"), style: .plain, target: self,action:#selector(MoviesViewController.openSearch))
        self.navigationItem.rightBarButtonItem  = buttonSearch
    }
    
    //MARK: Transitions
    
    func openSearch() {
    
        let vc = SearchViewController.storyboardInit()
        let navController = UINavigationController(rootViewController: vc)
        vc.firstSearchType = .actors
        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(navController, animated: true, completion: nil)
    }
}

extension ActorsViewController {
    
    //MARK: Actors Collection View delegate methods
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "ActorsHeader", for: indexPath)
        
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenWidth = Double(UIScreen.main.bounds.width)
        // space size
        let space = 17.0
        // spaces that are on right and left side of cell + one in the middle -> numberOfCellsInARow
        let numberOfSpaces = 3.0
        let numberOfCellsInARow = 2.0
        let width = (screenWidth - numberOfSpaces * space)/numberOfCellsInARow
        
        return CGSize(width: width, height: 249.0)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return actorsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActorCollectionViewCell", for: indexPath) as? ActorCollectionViewCell {
            
            let actor = actorsArray[indexPath.row]
            
            cell.actorName.text = actor.name
            cell.athorCharacter.text = actor.knownFor.first?.title ?? ""
            
            guard let imgPath = actor.imagePath else {
                
                return cell
            }
            
            guard let basePath = imageManager.baseImageUrl(imagesize: Int( cell.actorPhoto.frame.width), type: .poster) else {
                return cell
            }
            
            let imageUrl =  URL(string: basePath + imgPath)
            
            guard let url = imageUrl else {
                print("wrong image url in movie cell")
                return cell
            }
            
            cell.actorPhoto.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "ActorPlaceholder"), filter: nil, progress: nil, progressQueue: .main, imageTransition: UIImageView.ImageTransition.crossDissolve(0.3), runImageTransitionIfCached: false, completion: nil)
            
            return cell
            
        } else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = ActorDetailViewController.storyboardInit()
        vc.actorId = String(describing: actorsArray[indexPath.row].actorId)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
