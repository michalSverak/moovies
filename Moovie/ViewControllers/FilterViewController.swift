//
//  FilterViewController.swift
//  Moovie
//
//  Created by Michal Sverak on 8/8/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, StoryboardInit {
    
    //MARK: Outlets

    @IBOutlet weak var tableView: UITableView!
    
    //MARK: Variables and constants
    
    var genres = GenreManager.shared.genresArray
    var selectedGenres = [MovieGenre]()
    let allGenresItem = MovieGenre(id: 0, name: "All Genres")
    var delegate: SelectedGenresDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add all genres item to genres
        genres.insert(allGenresItem, at: 0)
        
        //Setup view
        tableView.separatorColor = ColorPalette.separatorColor
        tableView.delegate = self
    }
    
    //MARK: Filter methods
    
    @IBAction func clearSelectionAction(_ sender: Any) {
        
        selectedGenres = [MovieGenre]()
        tableView.reloadData()
    }
    
    //MARK: Transitions
    
    @IBAction func dismiss(_ sender: Any) {
        
        delegate?.selectedGenres(genres: selectedGenres)
        self.dismiss(animated: true, completion: nil)
    }
}

extension FilterViewController {
    
    //MARK: Table view delegate methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return genres.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell") as?
            FilterTableViewCell else {
                
            print("Cannot deque FilterTableViewCell")
            return UITableViewCell()
        }
        
        let genre = genres[indexPath.row]
        
        //Set cell to default setting
        cell.genreLabel.text = genre.name
        cell.tintColor = .red
        cell.accessoryType = .none
        cell.genreLabel.textColor = UIColor(colorLiteralRed: 137/255.0, green: 137/255.0, blue: 138/255.0, alpha: 1.0)
        
        //If the cell should be selected it, do so.
        
        //When selectedGenres array is empty it means no filter is applied and so all genres are selected
        if selectedGenres.isEmpty && genre.name == allGenresItem.name {
            cell.genreLabel.textColor = .white
            cell.accessoryType = .checkmark
        }
        
        // Select all genres contained in selectedGenre array
        if selectedGenres.contains(where: { $0.id == genre.id }) {
            cell.genreLabel.textColor = .white
            cell.accessoryType = .checkmark
        }
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let cell = tableView.cellForRow(at: indexPath) as? FilterTableViewCell else {
            print("Cannot deque FilterTableViewCell")
            return
        }
        
        if cell.accessoryType == .checkmark {
            
            //Deselect item
            cell.accessoryType = .none
            cell.genreLabel.textColor = ColorPalette.unselectedGenre
            let selectedItem = genres[indexPath.row]
            if let index = selectedGenres.index(where: { $0.id == selectedItem.id }) {
                selectedGenres.remove(at: index)
            }
            
        } else {
            
            //Select item
            cell.accessoryType = .checkmark
            cell.genreLabel.textColor = .white
            
            let selectedItem = genres[indexPath.row]
            
            guard selectedItem.name != allGenresItem.name else {
                
                //When user selects All Genres empty selectedGenres array.
                selectedGenres = [MovieGenre]()
                tableView.reloadData()
                return
            }
            
            // Whenever user selects a genre deselect All genres row
            let allGenresIndexPath = IndexPath(row: 0, section: 0)
            if let allGenresCell = tableView.cellForRow(at: allGenresIndexPath) as? FilterTableViewCell {
                allGenresCell.accessoryType = .none
                allGenresCell.genreLabel.textColor = ColorPalette.unselectedGenre
            }
            
            self.selectedGenres.append(selectedItem)
        }
    }
}
