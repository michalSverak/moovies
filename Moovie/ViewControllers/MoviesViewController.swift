//
//  MoviesViewController.swift
//  Moovie
//
//  Created by Michal Sverak on 7/27/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol SelectedGenresDelegate {
    func selectedGenres(genres: [MovieGenre])
}

class MoviesViewController: UIViewController, UIScrollViewDelegate, StoryboardInit, ShowDetailDelegate, MoviesViewModelDelegate, SelectedGenresDelegate, AlertPresenter {
    
    //MARK: Outlets
    
    @IBOutlet weak var movieSlidesScrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var movieNameLabel: UILabel!
    @IBOutlet weak var movieGenreLabel: UILabel!
    @IBOutlet weak var mostPopularMoviesCollectionView: MoviesCollection!
    @IBOutlet weak var nowPlayingMoviesCollectionView: MoviesCollection!
    @IBOutlet weak var nowPlayingHeight: NSLayoutConstraint!
    @IBOutlet weak var mostPopularHeight: NSLayoutConstraint!
    @IBOutlet weak var movieSlidesHeight: NSLayoutConstraint!
    
    //MARK: Variables and constants
    
    var movies = [FeaturedMovieStub]()
    var currentSlide = 0
    var viewModel: MoviesViewModel!
    let imageManager = ImageSizeManager.shared
    let reachabilityManager = ReachabilityManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set Collection views delegates
        mostPopularMoviesCollectionView.delegate = self
        nowPlayingMoviesCollectionView.delegate = self
        
        //Initialize ViewModel + load data
        viewModel = MoviesViewModel.resolve()
        viewModel.delegate = self
        reloadData()
        
        //Setup view
        navBarSetup()
        mostPopularMoviesCollectionView.backgroundColor = UIColor.clear
        nowPlayingMoviesCollectionView.backgroundColor = UIColor.clear
        
        //Add observer for didBecomeActive
        
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(reloadData),
                         name:  NSNotification.Name(rawValue: "UIApplicationDidBecomeActiveNotification"),
                         object: nil)
        
        //Add Reachability listener
        
        observeReachability()
    }
    
    //MARK: data reload
    
    func reloadData() {
        SVProgressHUD.show()
        viewModel.getData()
    }
    
    //MARK: Reachability 

    func observeReachability() {
        
        reachabilityManager.observer?.listener = { [weak self] status in
            
            switch status {
            case .reachable(.ethernetOrWiFi):
                self?.reloadData()
            case .reachable(.wwan):
                self?.reloadData()
            default:
                return
            }
        }
        
        reachabilityManager.observer?.startListening()
    }
    
    //MARK: ViewModel delegate methods

    func dataDownloaded() {
        
        SVProgressHUD.dismiss(withDelay: AppSettings.progressHUDDelay)
        
        mostPopularMoviesCollectionView.dataArray = viewModel.mostPopularMovies
        mostPopularMoviesCollectionView.collectionView.reloadData()

        movies = Array(viewModel.mostPopularMovies.prefix(5))
    
        scrollViewSlidesSetup()
    
        nowPlayingMoviesCollectionView.dataArray = viewModel.nowPlayingMovies
        nowPlayingMoviesCollectionView.collectionView.reloadData()
    
        self.movieNameLabel.text = movies.first?.originalTitle ?? ""
        self.movieGenreLabel.text = movies.first?.genres.first?.uppercased() ?? ""
    }
    
    //Error handling
    
    func handleError(_ error: Error) {
        
        SVProgressHUD.dismiss(withDelay: AppSettings.progressHUDDelay)

        self.showAlert(title: error.localizedDescription, message: error.localizedDescription, actionTitle: "Try Again") {
            
            self.viewModel.getData()
        }
    }
    
    //MARK: View setup methods

    func navBarSetup() {
        
        //self.navigationController?.hidesBarsOnSwipe = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        let buttonFilter = UIBarButtonItem(image: #imageLiteral(resourceName: "Settings"), style: .plain, target: self,action:#selector(MoviesViewController.openFilter))
        self.navigationItem.leftBarButtonItem  = buttonFilter
        
        let buttonSearch = UIBarButtonItem(image: #imageLiteral(resourceName: "Search"), style: .plain, target: self,action:#selector(MoviesViewController.openSearch))
        self.navigationItem.rightBarButtonItem  = buttonSearch
    }
    
    //MARK: Navbar button actions
    
    func openSearch() {
      
        let vc = SearchViewController.storyboardInit()
        let navController = UINavigationController(rootViewController: vc)
        vc.firstSearchType = .movies
        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(navController, animated: true, completion: nil)
    }
    
    func openFilter() {
        
        let vc = FilterViewController.storyboardInit()
        vc.delegate = self
        vc.selectedGenres = viewModel.selectedGenres
        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK: SelectedGenresDelegate method
    
    func selectedGenres(genres: [MovieGenre]) {
        SVProgressHUD.show()
        viewModel.setGenres(genres: genres)
    }
    
    //MARK: ShowDetailDelegate method
    
    func didSelectMovie(withId: Int, movie: FeaturedMovieStub) {

        let vc = MovieDetailViewController.storyboardInit()
        vc.currentMovieId = withId
        vc.movieStub = movie
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MoviesViewController {
    
    //MARK: Featured movies scroll view methods
    
    func scrollViewSlidesSetup() {
        
        let scrollViewWidth: CGFloat = self.view.bounds.width
        let scrollViewHeight: CGFloat = self.movieSlidesScrollView.bounds.height
        
        //Set scroll view to 1st slide
        self.movieSlidesScrollView.contentOffset = CGPoint(x: 0.0, y: 0.0)
        
        // Remove all slides before setting new ones
        self.movieSlidesScrollView.subviews.forEach({ $0.removeFromSuperview() })

        // Create Image view for every page of scroll view
        for i in 0..<movies.count {
        
            let imageView = UIImageView(frame: CGRect(x:scrollViewWidth * CGFloat(i), y:0,width:scrollViewWidth, height:scrollViewHeight))

            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            
            guard let basePath = imageManager.baseImageUrl(imagesize: Int(imageView.frame.width), type: .backdrop) else {
                return
            }
            
            let imageUrl =  URL(string: basePath + movies[i].imagePath)
            
            guard let url = imageUrl else {
                print("wrong image url in movie cell")
                return
            }
            
            imageView.alpha = 0.67
            
            imageView.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "movieBackdropPlaceholder"), filter: nil, progress: nil, progressQueue: .main, imageTransition: UIImageView.ImageTransition.crossDissolve(0.3), runImageTransitionIfCached: false, completion: nil)
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(MoviesViewController.slideTapped))
            imageView.addGestureRecognizer(tap)
            imageView.isUserInteractionEnabled = true
            
            self.movieSlidesScrollView.addSubview(imageView)
        }
        
        self.movieSlidesScrollView.contentSize = CGSize(width: scrollViewWidth * CGFloat(movies.count), height:self.movieSlidesScrollView.frame.height)
        self.movieSlidesScrollView.delegate = self
        self.pageControl.numberOfPages = movies.count
        self.pageControl.currentPage = 0
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == movieSlidesScrollView {
        
            let pageWidth: CGFloat = scrollView.frame.width
            let currentPage: CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
            
            self.pageControl.currentPage = Int(currentPage)
            self.currentSlide = Int(currentPage)
            
            let currentItem = movies[Int(currentPage)]
            self.movieNameLabel.text = currentItem.originalTitle
            self.movieGenreLabel.text = currentItem.genres.first?.uppercased()
        }
    }
    
    func slideTapped() {
        
        // Pass movie id to movie detail view
        let selectedItem = movies[currentSlide]
        let vc = MovieDetailViewController.storyboardInit()
        vc.currentMovieId = selectedItem.movieId
        vc.movieStub = movies[currentSlide]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
