//
//  AuthenticationViewController.swift
//  Moovie
//
//  Created by Michal Sverak on 9/7/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

class AuthenticationViewController: UIViewController, UIWebViewDelegate, StoryboardInit,AuthenticationViewModelProtocol, AlertPresenter {
    
    //MARK: Outlets
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: Variables
    
    var token: Token?
    var viewModel: AuthenticationViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Initial View setup
        
        navBarSetup()
        
        //ViewModel initialization
        
        viewModel = AuthenticationViewModel.resolve()
        viewModel.delegate = self
        
        //WebView initialization
        
        loadAuthentication()
        webView.delegate = self
    }
    
    //MARK: ViewModel delegate methods
    
    //Error handling
    
    func handleError(_ error: Error) {

        self.showAlert(title: error.localizedDescription, message: error.localizedDescription, actionTitle: "Try again") { [weak self] in
            
            guard let authToken = self?.token?.token else {
                print("token is nil")
                return
            }

            self?.viewModel.requestSessionID(token: authToken)
        }
    }
    
    //MARK: Authentication methods
    
    func loadAuthentication() {
    
        guard let tokenUrl = token?.token else {
            return
        }
        
        let authUrl = URL(string: AppSettings.authenticationUrl + tokenUrl)
        
        guard let url = authUrl else {
            return
        }
        
        webView.loadRequest(URLRequest(url: url))
        
    }
    
    //MARK: Web View methods
    
    func webViewDidStartLoad(_ webView: UIWebView) {

        activityIndicator.isHidden = false
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        let currentURL = webView.stringByEvaluatingJavaScript(from: "window.location.href")!

        if currentURL.contains("allow") {
            
            guard let authToken = token?.token else {
                return
            }
            
            webView.isHidden = true
            viewModel.requestSessionID(token: authToken)
        }
        
        activityIndicator.isHidden = true
    }
    
    //MARK: View setup
    
    func navBarSetup() {
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        let buttonSearch = UIBarButtonItem(image: #imageLiteral(resourceName: "Cancel"), style: .plain, target: self,action:#selector(AuthenticationViewController.cancel))
        self.navigationItem.rightBarButtonItem  = buttonSearch
    }
    
    //MARK: Transitions
    
    func userIsAuthenticated() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func cancel() {
        
        self.dismiss(animated: true, completion: nil)
    }
}
