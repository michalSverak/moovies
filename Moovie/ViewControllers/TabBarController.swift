//
//  TabBarControllerController.swift
//  Moovie
//
//  Created by Michal Sverak on 8/8/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController, StoryboardInit {

    override func viewDidLoad() {
        super.viewDidLoad()
       
        let movies = MoviesViewController.storyboardInit()
        movies.tabBarItem = UITabBarItem(title: "Movies", image: #imageLiteral(resourceName: "MoviesTabBar"), tag: 0)
        
        let actors = ActorsViewController.storyboardInit()
        actors.tabBarItem = UITabBarItem(title: "Actors", image: #imageLiteral(resourceName: "ActorsTabBar"), tag: 1)
        
        let profile = UserProfileViewController.storyboardInit()
        profile.tabBarItem = UITabBarItem(title: "Profile", image: #imageLiteral(resourceName: "ProfileTabBar"), tag: 2)
        
        let viewControllerList = [ movies, actors, profile ]
        viewControllers = viewControllerList.map { UINavigationController(rootViewController: $0) }
        
        //Setup tab bar
        
        self.tabBar.barTintColor = ColorPalette.backgroundColor
        self.tabBar.isTranslucent = true
        self.tabBar.tintColor = .white
        self.tabBar.alpha = 0.8
    }
    
    override func viewWillLayoutSubviews() {
        var tabFrame = self.tabBar.frame
        tabFrame.size.height = AppSettings.tabBarHeight
        tabFrame.origin.y = self.view.frame.size.height - AppSettings.tabBarHeight
        self.tabBar.frame = tabFrame
    }
}
