//
//  PopUpDetailViewController.swift
//  Moovie
//
//  Created by Michal Sverak on 9/6/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit
import AlamofireImage
import SVProgressHUD

class PopUpDetailViewController: UIViewController, UIScrollViewDelegate, StoryboardInit, UIWebViewDelegate {
    
    //MARK: Outlets
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var imageDetail: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    //MARK: Variables and constants
    
    var background = UIImage()
    var imageUrl: String?
    var trailerKey: String?
    let imageManager = ImageSizeManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //View setup
        
        showImageDetail()
        scrollView.delegate = self
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 6.0
        automaticallyAdjustsScrollViewInsets = false
        
        //Decide whether view should be used for Image detail mode or for trailer video mode
        
        guard let key = trailerKey else {
            return
        }
        
        imageDetail.isHidden = true
        scrollView.isHidden = true
        playVideoWithId(videoId: key)
    }
    
    //MARK: View setup
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.backgroundImage.image = background
    }
    
    //MARK: Image detail mode methods
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        return self.imageDetail
    }
    
    func showImageDetail() {
        
        guard let imagePath = imageUrl else {
            return
        }
        
        guard let basePath = imageManager.baseImageUrl(imagesize: Int(imageDetail.frame.width), type: .original) else {
            return
        }
        
        let imgUrl =  URL(string: basePath + imagePath)
        
        guard let url = imgUrl else {
            print("wrong image url in image detail popUp")
            return
        }
        
        SVProgressHUD.show()
        
        imageDetail.af_setImage(withURL: url, placeholderImage: nil, filter: nil, progress: nil, progressQueue: .main, imageTransition: UIImageView.ImageTransition.crossDissolve(0.3), runImageTransitionIfCached: false) { response in
            
            SVProgressHUD.dismiss(withDelay: AppSettings.progressHUDDelay)
        }
    }
    
    @IBAction func tapGesture(_ sender: Any) {
        
        
        if (scrollView.zoomScale > scrollView.minimumZoomScale) {
            scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true)
        } else {
            scrollView.setZoomScale(scrollView.maximumZoomScale, animated: true)
        }
    }
    
    //MARK: Trailer video mode methods
    
    var youTubeVideoHTML: String = "<!DOCTYPE html><html><head><style>body{margin:0px 0px 0px 0px;}</style></head> <body> <div id=\"player\"></div> <script> var tag = document.createElement('script'); tag.src = \"http://www.youtube.com/player_api\"; var firstScriptTag = document.getElementsByTagName('script')[0]; firstScriptTag.parentNode.insertBefore(tag, firstScriptTag); var player; function onYouTubePlayerAPIReady() { player = new YT.Player('player', { width:'%0.0f', height:'%0.0f', videoId:'%@', events: { 'onReady': onPlayerReady, } }); } function onPlayerReady(event) { event.target.playVideo(); } </script> </body> </html>"
    
    func playVideoWithId(videoId: String) {
        
        let webView = UIWebView()
        webView.delegate = self

        webView.frame = CGRect(x: 0, y: self.view.center.y - 150 , width: UIScreen.main.bounds.width, height: 300)
        let html: String = String(format: youTubeVideoHTML, UIScreen.main.bounds.width, 300.0, videoId)
        webView.mediaPlaybackRequiresUserAction = false
        webView.loadHTMLString(html, baseURL: Bundle.main.resourceURL)
        webView.scrollView.isScrollEnabled = false
        webView.scrollView.bounces = false
        webView.isOpaque = false
        webView.backgroundColor = .clear
        self.view.addSubview(webView)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        SVProgressHUD.show()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss(withDelay: AppSettings.progressHUDDelay)
    }
    
    //MARK: Transitions
    
     @IBAction func closePopUp(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
