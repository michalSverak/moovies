//
//  ActorMoviesViewController.swift
//  Moovie
//
//  Created by Michal Sverak on 8/18/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

class ActorMoviesViewController: UITableViewController, StoryboardInit {
    
    //MARK: Variables and constants
    
    var dataSource: [FeaturedMovieStub]?
    var isScrollEnabled: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //This VC is used for acting section in ActorDetail and also in show all acting.
        //Paging doesnt need scroll but show all does therefore scroll is enabled/disbaled here.
        
        guard let scroll = isScrollEnabled else {
            return
        }
        
        self.tableView.isScrollEnabled = scroll
    }

    // MARK: Table view delegate methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let count = dataSource?.count else {
            return 0
        }
        
        return count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let data = dataSource else {
            return UITableViewCell()
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ActorMoviesTableViewCell", for: indexPath) as? ActorMoviesTableViewCell else {
            
            return UITableViewCell()
        }
        
        let movie = data[indexPath.row]
        
        cell.movieTitleLabel.text = movie.originalTitle
        
        if let year = movie.releaseDate {
            cell.yearLabel.text = year
        }
        
        guard movie.character != "" else {
            cell.movieTitleLabel.text = movie.originalTitle
            return cell
        }
        
        let character = "as \(movie.character)"
        let actingString = "\(movie.originalTitle) \(character)"
        let range = NSString(string: actingString).range(of: character)
        let attributedString = NSMutableAttributedString(string: actingString)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: ColorPalette.characterGrey, range: range)
        
        cell.movieTitleLabel.attributedText = attributedString
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        guard let movie = dataSource?[indexPath.row] else {
            return
        }
        
        let vc = MovieDetailViewController.storyboardInit()
        vc.currentMovieId = movie.movieId
        self.navigationController?.pushViewController(vc,animated:true)
    }
}
