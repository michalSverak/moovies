//
//  AllCommentsTableViewController.swift
//  Moovie
//
//  Created by Michal Sverak on 8/31/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

class AllCommentsTableViewController: UITableViewController, StoryboardInit {
    
    //MARK: Variabled and constants
    
    var comments = [MovieComment]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setup comment table view cell
        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        //register comment table view cell from nib
        let bundle = Bundle(for: type(of: self))
        let commentCell = UINib(nibName: "CommentTableViewCell", bundle: bundle)
        tableView.register(commentCell, forCellReuseIdentifier: "CommentTableViewCell")
    }
    
    //MARK: Comments Table View delegate methods
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return comments.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell", for: indexPath) as? CommentTableViewCell else {
            return UITableViewCell()
        }
        
        let comment = comments[indexPath.row]
        cell.nameLabel.text = comment.author
        cell.commentText.text = comment.content
        
        return cell
    }
}
