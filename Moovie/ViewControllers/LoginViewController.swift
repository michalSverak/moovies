//
//  LoginViewController.swift
//  Moovie
//
//  Created by Michal Sverak on 8/7/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, StoryboardInit, LoginViewViewModelProtocol, AlertPresenter {
    
    //MARK: Outlets
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: variables
    
    var viewModel: LoginViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Initial view setup
        navBarSetup()
        animateLogo()
        activityIndicator.isHidden = true
        
        //ViewModel initialization
        viewModel = LoginViewModel.resolve()
        viewModel.delegate = self
    }
    
    //MARK: ViewModel Delegate methods
 
    func tokenRecieved() {
        
        activityIndicator.isHidden = true
        
        guard let token = viewModel.token else {
            
            print("token is nil")
            return
        }
        
        let vc = AuthenticationViewController.storyboardInit()
        vc.token = token
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //Error handling
  
    func handleError(_ error: Error) {
        
        self.showAlert(title: error.localizedDescription, message: error.localizedDescription,actionTitle: "OK", ButtonAction: nil)
    }
    
    //MARK: View setup + initial animation
    
    func animateLogo() {
        UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseOut, animations: {
            self.logo.transform = CGAffineTransform(translationX: 0, y: -100)
        }, completion: nil)
    }
    
    func navBarSetup() {
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        let buttonSearch = UIBarButtonItem(image: #imageLiteral(resourceName: "Cancel"), style: .plain, target: self,action:#selector(LoginViewController.cancel))
        self.navigationItem.rightBarButtonItem  = buttonSearch
    }
    
    //MARK: Transitions

    @IBAction func login(_ sender: Any) {
        
        activityIndicator.isHidden = false
        viewModel.requestToken()
    }
    
    func cancel() {
        
        self.dismiss(animated: true, completion: nil)
    }
}
