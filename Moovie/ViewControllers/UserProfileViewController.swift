//
//  UserProfileViewController.swift
//  Moovie
//
//  Created by Michal Sverak on 8/8/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit
import AlamofireImage
import SVProgressHUD

class UserProfileViewController: UIViewController,StoryboardInit, ShowDetailDelegate, UserProfileViewModelDelegate, AlertPresenter {
    
    //MARK: Outlets
    
    @IBOutlet weak var userProfile: ImageViewWithFade!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var favoriteMovies: ShowAllwithLabelView!
    @IBOutlet weak var favoriteMoviesCollectionView: MoviesCollection!
    
    //MARK: Variables and constants
    
    var viewModel: UserProfileViewModel!
    let reachabilityManager = ReachabilityManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Initial view setup
        
        favoriteMoviesCollectionView.delegate = self
        setupView()
        userNameLabel.text = ""
        favoriteMovies.showAllButton.isHidden = true
        
        //View model initialization
        
        viewModel = UserProfileViewModel.resolve()
        viewModel.delegate = self
        
        //View setup based on user login
        
        navBarSetup()
        
        guard viewModel.isUserLoggedIn() else {
            userNotLoggedIn()
            return
        }
        
        SVProgressHUD.show()
        viewModel.getUser()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        guard viewModel.isUserLoggedIn() else {
            userNotLoggedIn()
            return
        }
    
       reloadView()
    }

    //MARK: View Setup
    
    func updateView() {
        
         SVProgressHUD.dismiss(withDelay: AppSettings.progressHUDDelay)
        
        guard let user = viewModel.user else {
            return
        }
        
        userNameLabel.text = setUsername(user: user)
        setImage(user: user)
        
        viewModel.getUserFavoriteMovies()
    }
    
    func userFavoriteMoviesReady() {
        
        favoriteMoviesCollectionView.dataArray = viewModel.userFavoriteMovies
        favoriteMoviesCollectionView.collectionView.reloadData()
    }
    
    func setUsername(user: User) -> String {
        
        if let name = user.name {
            if name != "" {
                return name.capitalizingFirstLetter()
            }
        }

        if let username = user.username {
            if username != "" {
                return username.capitalizingFirstLetter()
            }
        }

        return ""
    }
    
    func setImage(user: User) {
        
        guard let avatarUrl = user.avatarUrl else {
            return
        }
    
        let path = AppSettings.gravatarUrl + avatarUrl
        let imageUrl =  URL(string: path)
        
        guard let url = imageUrl else {
            print("wrong image url in movie cell")
            return
        }
    
        userProfile.af_setImage(withURL: url, placeholderImage: nil , filter: nil, progress: nil, progressQueue: .main, imageTransition: UIImageView.ImageTransition.crossDissolve(0.3), runImageTransitionIfCached: false) { image in
        
            self.userProfile.blurImage()

        }
    }
 
    func setupView() {
        
        favoriteMovies.sectionTitleLabel.text = "Favorite Movies"
        favoriteMoviesCollectionView.backgroundColor = .clear
        favoriteMovies.backgroundColor = .clear
    }
    
    func navBarSetup() {
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        guard viewModel.isUserLoggedIn() else {
            return
        }
        
        let buttonLogout = UIBarButtonItem(image: #imageLiteral(resourceName: "Options"), style: .plain, target: self,action:#selector(UserProfileViewController.logoutActionSheet))
        self.navigationItem.rightBarButtonItem  = buttonLogout
    }
    
    //MARK: Error handling
    
    func handleError(_ error: Error) {
        
        SVProgressHUD.dismiss(withDelay: AppSettings.progressHUDDelay)
        
        self.showAlert(title: error.localizedDescription, message: error.localizedDescription, actionTitle: "Try Again") {
            
            self.viewModel.getUser()
        }
    }
    
    //MARK: Reachability
    
    func observeReachability() {
        
        reachabilityManager.observer?.listener = { [weak self] status in
            
            switch status {
            case .reachable(.ethernetOrWiFi):
                self?.reloadView()
            case .reachable(.wwan):
                self?.reloadView()
            default:
                return
            }
        }
        
        reachabilityManager.observer?.startListening()
    }
    
    //MARK: Reload data
    
    func reloadView() {
        
        SVProgressHUD.show()
        viewModel.getUserFavoriteMovies()
        updateView()
        navBarSetup()
    }

    //MARK: Movie Detail
    
    func didSelectMovie(withId: Int, movie: FeaturedMovieStub) {
        
        let vc = MovieDetailViewController.storyboardInit()
        vc.currentMovieId = withId
        vc.movieStub = movie
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: User Log in methods
    
    func userNotLoggedIn() {
        
        userNameLabel.text = "User Profile"
        
        self.showAlert(title: "You are not logged in",
                       message: "To be able to use full features of Moovies you need to log in. Would you like to do so? ", actionTitle: "Log in") {
                        
                        let vc = LoginViewController.storyboardInit()
                        let navController = UINavigationController(rootViewController: vc)
                        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        self.present(navController, animated: true, completion: nil)
        }
    }
    
    func logoutActionSheet() {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let logout = UIAlertAction(title: "Log out", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction!) -> Void in self.logOutUser() })
        logout.setValue(ColorPalette.buttonRed, forKey: "titleTextColor")
        actionSheet.addAction(logout)
        
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        cancel.setValue(ColorPalette.buttonGrey, forKey: "titleTextColor")
        actionSheet.addAction(cancel)

        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func logOutUser() {
        
        // Delete all user related data
        
        viewModel.keychain.clearKeychain()
        viewModel.userDefaultsStorage.deleteAllUserDefaults()
        
        // Set view to initial state
        
        userNameLabel.text = "User Profile"
        userProfile.image = nil
        self.navigationItem.rightBarButtonItem = nil
        favoriteMoviesCollectionView.dataArray = [FeaturedMovieStub]()
        favoriteMoviesCollectionView.collectionView.reloadData()
        
        // Take user to initial tab
        
        if let destinationViewController = tabBarController?.viewControllers?[0] {
        
            let overlayView = UIScreen.main.snapshotView(afterScreenUpdates: false)
            destinationViewController.view.addSubview(overlayView)
            
            UIView.animate(withDuration: 0.5, delay: 0, options: .transitionCrossDissolve, animations: {
                self.tabBarController?.selectedIndex = 0
                overlayView.alpha = 0
            }, completion: { finished in
                overlayView.removeFromSuperview()
            })
        }
    }
}
