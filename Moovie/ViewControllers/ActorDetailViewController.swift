//
//  ActorDetailViewController.swift
//  Moovie
//
//  Created by Michal Sverak on 8/8/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit
import SVProgressHUD

class ActorDetailViewController: UIViewController, StoryboardInit, ShowDetailDelegate, ActorDetailViewModelDelegate, ActorMoviesPagesDelegate, AlertPresenter {
    
    //MARK: Outlets
    
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bioLabel: UILabel!
    @IBOutlet weak var birthdayLabel: UILabel!
    @IBOutlet weak var placeOfBirth: UILabel!
    @IBOutlet weak var actorImage: ImageViewWithFade!
    @IBOutlet weak var moviesPageControl: UIPageControl!
    @IBOutlet weak var knownFor: ShowAllwithLabelView!
    @IBOutlet weak var acting: ShowAllwithLabelView!
    @IBOutlet weak var knownForCollectionView: MoviesCollection!
    @IBOutlet weak var actorMovies: UIView!
    
    //MARK: Variables and constants
    
    var vc: ActorMoviesPagingController!
    var viewModel: ActorDetailViewModel!
    var actorId: String!
    let imageManager = ImageSizeManager.shared
    let reachabilityManager = ReachabilityManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //ViewModel initialization
        viewModel = ActorDetailViewModel.resolve()
        viewModel.delegate = self
        
        //Initial data load
        reloadData()
        
        //Setup View
        
        setupView()
        knownForCollectionView.delegate = self
        vc = ActorMoviesPagingController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        vc.currentPageDelegate = self
        moviesPageControl.numberOfPages = vc.numberOfPages
        
        //Observe reachability
        observeReachability()
    }

    //MARK: ActorDetailViewModelDelegate methods
    
    func dataUpdated() {
        
        SVProgressHUD.dismiss(withDelay: AppSettings.progressHUDDelay)
        
        guard let actor = viewModel.actorDetail else {
            return
        }
        
        nameLabel.text = actor.name
        bioLabel.text = actor.bio
        birthdayLabel.text = actor.birthday
        if actor.birthday == "" {
            birthdayLabel.isHidden = true
        }
        placeOfBirth.text = actor.placeOfBirth
        setImageForTopView()
        knownForCollectionView.dataArray = actor.movies
        genderLabel.text = actor.gender
        knownForCollectionView.collectionView.reloadData()
        
        embedPagingController()
        moviesPageControl.numberOfPages = vc.numberOfPages
        vc.currentPageDelegate = self
    }
    
    //Error handling
    
    func handleError(_ error: Error) {
        
        SVProgressHUD.dismiss(withDelay: AppSettings.progressHUDDelay)
        
        self.showAlert(title: error.localizedDescription, message: error.localizedDescription, actionTitle: "Try Again") {
            self.reloadData()
        }
    }
    
    //MARK: Data reload
    
    func reloadData() {
        
        SVProgressHUD.show()
        viewModel.getActorDetail(id: self.actorId)
        
    }
    
    //MARK: Reachability
    
    func observeReachability() {
        
        reachabilityManager.observer?.listener = { [weak self] status in
            
            guard self?.actorId != nil else {
                return
            }
            
            switch status {
            case .reachable(.ethernetOrWiFi):
                self?.reloadData()
            case .reachable(.wwan):
                self?.reloadData()
            default:
                return
            }
        }
        
        reachabilityManager.observer?.startListening()
    }
    
    //MARK: Acting paging controller methods
    
    func embedPagingController() {

        if let movies = viewModel.actorDetail?.movies {
            
            // Do not create more than 5 pages where each page contains 6 movies
            // => do not use more than 30 items
            let numberOfItemsForPaging = 30
            
            if movies.count <= numberOfItemsForPaging {
                acting.showAllButton.isHidden = true
                vc.movies = movies

            } else {
            
                acting.showAllButton.isHidden = false
                vc.movies = Array(movies.prefix(numberOfItemsForPaging))
            }
        }
        
        //add as a childviewcontroller
        addChildViewController(vc)
        
        // Add the child's View as a subview
        actorMovies.addSubview(vc.view)
        actorMovies.isUserInteractionEnabled = true
        vc.view.frame = actorMovies.bounds
        vc.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // tell the childviewcontroller it's contained in it's parent
        vc.didMove(toParentViewController: self)
    }
    
    //MARK: View setup methods
    
    func setImageForTopView() {
        
        guard let actor = viewModel.actorDetail else {
            return
        }
        
        guard let imagePath = actor.imagePath else {
            return
        }
        
        guard let basePath = imageManager.baseImageUrl(imagesize: Int(actorImage.frame.width), type: .backdrop) else {
            return
        }
        
        let imageUrl =  URL(string: basePath + imagePath)
        
        guard let url = imageUrl else {
            return
        }
        
        actorImage.alpha = 0.68
        actorImage.af_setImage(withURL: url, placeholderImage: nil, filter: nil, progress: nil, progressQueue: .main, imageTransition: UIImageView.ImageTransition.crossDissolve(0.3), runImageTransitionIfCached: false, completion: nil)
    }
    
    func currentPage(page: Int) {
        moviesPageControl.currentPage = page
    }

    
    func setupView() {
        
        nameLabel.text = ""
        bioLabel.text = ""
        
        knownFor.sectionTitleLabel.text = "Known for"
        acting.sectionTitleLabel.text = "Acting"
        acting.showAllButton.addTarget(self, action: #selector(ActorDetailViewController.showAllActing), for: .touchUpInside)
        knownFor.showAllButton.isHidden = true
        acting.showAllButton.isHidden = true
        
        knownForCollectionView.backgroundColor = .clear
        acting.backgroundColor = .clear
        knownFor.backgroundColor = .clear
    }
    
    func showAllActing() {
        
        let vc = ActorMoviesViewController.storyboardInit()
        if let movies = viewModel.actorDetail {
            vc.dataSource = movies.movies
        }
        vc.isScrollEnabled = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: Transitions
    
    func didSelectMovie(withId: Int, movie: FeaturedMovieStub) {
        
        let vc = MovieDetailViewController.storyboardInit()
        vc.currentMovieId = withId
        vc.movieStub = movie
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
