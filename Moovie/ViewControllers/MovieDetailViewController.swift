//
//  MovieDetailViewController.swift
//  Moovie
//
//  Created by Michal Sverak on 7/31/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit
import SVProgressHUD

class MovieDetailViewController: UIViewController, StoryboardInit, MovieDetailViewModelDelegate, AlertPresenter {
    
    //MARK: Outlets
    
    @IBOutlet weak var movieDetailTopView: MovieDetailTopView!
    @IBOutlet weak var starsCollectionView: UICollectionView!
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    @IBOutlet weak var starsSection: ShowAllwithLabelView!
    @IBOutlet weak var trailersSection: ShowAllwithLabelView!
    @IBOutlet weak var gallerySection: ShowAllwithLabelView!
    @IBOutlet weak var reviewsSection: ShowAllwithLabelView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var commentTableView: UITableView!
    @IBOutlet weak var commentTableViewBackground: UIView!
    @IBOutlet weak var commetTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var trailersCollectionView: UICollectionView!
    
    //MARK: Variables and constants
    
    var currentMovieId = 0
    var currentMovie: Movie?
    var viewModel: MovieDetailViewModel!
    var movieStub: FeaturedMovieStub?
    let imageManager = ImageSizeManager.shared
    let reachabilityManager = ReachabilityManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let bundle = Bundle(for: type(of: self))
    
        commentTableView.delegate = self
        starsCollectionView.delegate = self
        galleryCollectionView.delegate = self
        trailersCollectionView.delegate = self
        
        //Initialize viewModel
        viewModel = MovieDetailViewModel.resolve()
        viewModel.delegate = self
        viewModel.movieId = currentMovieId
        
        //Initial Data load
        reloadData()
        
        //Register collection view cells
        let cell = UINib(nibName: "ActorCollectionViewCell", bundle: bundle)
        starsCollectionView.register(cell, forCellWithReuseIdentifier: "ActorCollectionViewCell")
        
        let trailerCell = UINib(nibName: "TrailerCollectionViewCell", bundle: bundle)
        trailersCollectionView.register(trailerCell, forCellWithReuseIdentifier: "TrailerCollectionViewCell")
        
        //Setup sections
        setupSections()
        
        //Setup comment table view
        commentTableView.estimatedRowHeight = 100.0
        commentTableView.rowHeight = UITableViewAutomaticDimension
        
        //register comment table view cell from nib
        let commentCell = UINib(nibName: "CommentTableViewCell", bundle: bundle)
        commentTableView.register(commentCell, forCellReuseIdentifier: "CommentTableViewCell")
        
        //Seup scroll view
        scrollView.contentInset = UIEdgeInsetsMake(0, 0, AppSettings.tabBarHeight, 0)
        
        //Setup reviews section show more button
        reviewsSection.showAllButton.addTarget(self, action:#selector(MovieDetailViewController.showAllComments), for: .touchUpInside)
        
        //Initial view setup
        navBarSetup()
        
        guard let movie = movieStub else {
            return
        }
        
        movieDetailTopView.setup(movieGenre: movie.genres.first?.uppercased() ?? "", movieName: movie.originalTitle, movieDescription: movie.overview, creators: [])
        movieDetailTopView.setRating(rating: CGFloat(movie.rating / 10.0))
        
        //Observe reachability
        
        observeReachability()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navBarSetup()
    }
    
    //MARK: Data reload
    
    func reloadData() {
        
        SVProgressHUD.show()
        viewModel.getData()
    }
    
    //MARK: MovieDetailViewModelDelegate methods
    
    func dataUpdated() {
        
        SVProgressHUD.dismiss(withDelay: AppSettings.progressHUDDelay)
        
        guard let movie = viewModel.movieDetail else {
            return
        }
        
        currentMovie = movie
        
        var creators = [String]()
        for job in movie.crew {
            
            if job.job == "Director" {
                creators.append(job.name)
            }
        }
        
        movieDetailTopView.creators.text = movieDetailTopView.setupCreators(creators: creators)
        
        if movieDetailTopView.movieDescription.text == "" {
            movieDetailTopView.movieDescription.text = movie.overview
        }
        
        if movieDetailTopView.movieName.text == "" {
              movieDetailTopView.movieName.text = movie.originalTitle
        }
        
        movieDetailTopView.setRating(rating: CGFloat(movie.voteAverage! / 10.0))
        
        setImageForTopView()
        
        self.starsCollectionView.reloadData()
        self.commentTableView.reloadData()
        self.galleryCollectionView.reloadData()
        self.trailersCollectionView.reloadData()
        setTableViewHeight()
    }
    
    //Error handling
    
    func handleError(_ error: Error) {
        
        SVProgressHUD.dismiss(withDelay: AppSettings.progressHUDDelay)

        self.showAlert(title: error.localizedDescription, message: error.localizedDescription, actionTitle: "Try Again") {
            self.reloadData()
        }
    }
    
    //MARK: Reachability
    
    func observeReachability() {
        
        reachabilityManager.observer?.listener = { [weak self] status in
            
            switch status {
            case .reachable(.ethernetOrWiFi):
                self?.reloadData()
            case .reachable(.wwan):
                self?.reloadData()
            default:
                return
            }
        }
        
        reachabilityManager.observer?.startListening()
    }
    
    //MARK: View setup
    
    func navBarSetup() {
        
        var image = #imageLiteral(resourceName: "heart")
        
        if viewModel.isMovieFavorite() {
            image = #imageLiteral(resourceName: "heartFull")
        }
        
        let markAsFavorite = UIBarButtonItem(image: image, style: .plain, target: self,action:#selector(MovieDetailViewController.markAsFavorite))
        self.navigationItem.rightBarButtonItem  = markAsFavorite
    }
    
    //MARK: Movie detail methods
    
    func markAsFavorite() {
        
        guard viewModel.isUserLoggedIn() else {
            
            userNotLoggedIn()
            return
        }
        
        var favorite = true
        
        if viewModel.isMovieFavorite() {
            favorite = false
        }
        
        guard let id = currentMovie?.movieId else {
            print("current movie id is nil")
            return
        }
        
        viewModel.markAsFavorite(movieID: id, favorite: favorite)
    }
    
    func userNotLoggedIn() {
        
        self.showAlert(title: "You are not logged in",
                       message: "To be able to mark movie as favorite you need to be logged in. Would you like to do so? ", actionTitle: "Log in") {
                        
            let vc = LoginViewController.storyboardInit()
            let navController = UINavigationController(rootViewController: vc)
            vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    func updateFavorite() {
        
        var image = #imageLiteral(resourceName: "heart")
        
        if viewModel.isMovieFavorite() {
            image = #imageLiteral(resourceName: "heartFull")
        }
        
        self.navigationItem.rightBarButtonItem?.image = image
    }
    
    func setTableViewHeight() {
        
        UIView.animate(withDuration: 0, animations: {
            self.commentTableView.layoutIfNeeded()
        }) { (complete) in
            
            var heightOfTableView: CGFloat = 0.0
            
            // Get visible cells and sum up their heights
            let cells = self.commentTableView.visibleCells
            
            let count = self.currentMovie?.comments?.count ?? 0
            
            if count > self.commentTableView.visibleCells.count {
                self.reviewsSection.showAllButton.isHidden = false
            }
            
            for cell in cells {
                heightOfTableView += cell.frame.height
            }
            
            // Edit heightOfTableViewConstraint's constant to update height of table view
            if heightOfTableView == 0 {
                //Movie has no comments
                heightOfTableView = 60.0
            }
            self.commetTableViewHeight.constant = heightOfTableView
        }
    }

    func setImageForTopView() {
        
        guard let movie = viewModel.movieDetail else {
            return
        }
        
        var imagePath: String?
        
        if let poster = movie.posterPath {
            imagePath = poster
        }
        
        if let backdrop = movie.imagePath {
            imagePath = backdrop
        }
        
        guard let imgPath = imagePath else {
            return
        }
        
        guard let basePath = imageManager.baseImageUrl(imagesize: Int(movieDetailTopView.movieImage.frame.width), type: .backdrop) else {
            return
        }
        
        let imageUrl =  URL(string: basePath + imgPath)
        
        guard let url = imageUrl else {
            return
        }
        
        movieDetailTopView.movieImage.alpha = 0.68
        movieDetailTopView.movieImage.af_setImage(withURL: url, placeholderImage: nil, filter: nil, progress: nil, progressQueue: .main, imageTransition: UIImageView.ImageTransition.crossDissolve(0.3), runImageTransitionIfCached: false, completion: nil)
    }
    
    func setupSections() {
        
        // Setup section titles
        starsSection.sectionTitleLabel.text = "Stars"
        trailersSection.sectionTitleLabel.text = "Trailers"
        gallerySection.sectionTitleLabel.text = "Gallery"
        reviewsSection.sectionTitleLabel.text = "Reviews"
        
        reviewsSection.showAllButton.isHidden = true
        starsSection.showAllButton.isHidden = true
        trailersSection.showAllButton.isHidden = true
        gallerySection.showAllButton.isHidden = true
        
        starsSection.backgroundColor = .clear
        trailersSection.backgroundColor = .clear
        gallerySection.backgroundColor = .clear
        reviewsSection.backgroundColor = .clear
    }
}

extension MovieDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    // Collection View delegate methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var numberOfItems = 0
        collectionView.backgroundView = nil
        
        let label = UILabel()
        label.center = collectionView.center
        label.font = UIFont(name: "Calibre", size: 16)
        label.textAlignment = .center
        label.textColor = ColorPalette.inactiveButtonColor
        
        switch collectionView {
        case starsCollectionView:
            
            numberOfItems = currentMovie?.actors.count ?? 0
            
            if numberOfItems == 0 {
                label.text = "No actors here yet..."
                collectionView.backgroundView = label
            }
            
        case galleryCollectionView:
            
            numberOfItems = currentMovie?.images?.count ?? 0
            
            if numberOfItems == 0 {
                label.text = "No images here yet..."
                collectionView.backgroundView = label
            }
            
        case trailersCollectionView:
            
            numberOfItems = currentMovie?.videos?.count ?? 0
            
            if numberOfItems == 0 {
                label.text = "No trailers here yet..."
                collectionView.backgroundView = label
            }
            
        default:
            numberOfItems = 0
        }
        
        return numberOfItems
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case starsCollectionView:
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActorCollectionViewCell", for: indexPath) as? ActorCollectionViewCell {
                
                guard let movie = viewModel.movieDetail else {
                    return cell
                }
                
                let actor = movie.actors[indexPath.row]
                
                cell.actorName.text = actor.name
                cell.athorCharacter.text = actor.character
                cell.actorPhoto.image = #imageLiteral(resourceName: "ActorPlaceholder")
                
                guard let imgPath = actor.imagePath else {
                    return cell
                }
                
                guard let basePath = imageManager.baseImageUrl(imagesize: Int(cell.actorPhoto.frame.width), type: .profile) else {
                    return cell
                }
                
                let imageUrl =  URL(string: basePath + imgPath)
                
                guard let url = imageUrl else {
                    print("wrong image url in actor cell")
                    return cell
                }
                
                cell.actorPhoto.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "ActorPlaceholder"), filter: nil, progress: nil, progressQueue: .main, imageTransition: UIImageView.ImageTransition.crossDissolve(0.3), runImageTransitionIfCached: false, completion: nil)
                
                return cell
            }
            
        case galleryCollectionView:
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCollectionViewCell", for: indexPath) as? GalleryCollectionViewCell {
                
                guard let imgPath = currentMovie?.images?[indexPath.row].imagePath else {
                    return cell
                }
                
                guard let basePath = imageManager.baseImageUrl(imagesize: Int(cell.galleryItem.frame.width), type: .backdrop) else {
                    return cell
                }
                
                let imageUrl =  URL(string: basePath + imgPath)
                
                guard let url = imageUrl else {
                    print("wrong image url in gallery cell")
                    return cell
                }
                
                cell.galleryItem.af_setImage(withURL: url, placeholderImage: nil, filter: nil, progress: nil, progressQueue: .main, imageTransition: UIImageView.ImageTransition.crossDissolve(0.3), runImageTransitionIfCached: false, completion: nil)
                
                return cell
            }
        case trailersCollectionView:
            
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrailerCollectionViewCell", for: indexPath) as? TrailerCollectionViewCell {
                
                guard let key = currentMovie?.videos?[indexPath.row].key else {
                    return cell
                }
                
                let path = AppSettings.youtubeThumbnailImagePath(key: key)
                let imageUrl =  URL(string: path)
                
                guard let url = imageUrl else {
                    print("wrong image url in trailer cell")
                    return cell
                }
                
                cell.trailerThumbnail.af_setImage(withURL: url, placeholderImage: nil, filter: nil, progress: nil, progressQueue: .main, imageTransition: UIImageView.ImageTransition.crossDissolve(0.3), runImageTransitionIfCached: false, completion: nil)
                
                return cell
            }

        default:
            return UICollectionViewCell()
        }

        return UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch collectionView {
        case starsCollectionView:
            let vc = ActorDetailViewController.storyboardInit()
            let actor = viewModel.movieDetail?.actors[indexPath.row]
            vc.actorId = String(describing: actor!.actorId)
            self.navigationController?.pushViewController(vc, animated: true)
        case trailersCollectionView:
            
            let vc = PopUpDetailViewController.storyboardInit()
            vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            let screenshot: UIImage = UIImage.takeScreenshot(view: self.view.window!)!
            vc.background = screenshot
            vc.trailerKey = currentMovie?.videos?[indexPath.row].key
            self.present(vc, animated: true, completion: nil)
        case galleryCollectionView:
            let vc = PopUpDetailViewController.storyboardInit()
            vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            let screenshot: UIImage = UIImage.takeScreenshot(view: self.view.window!)!
            vc.background = screenshot
            vc.imageUrl = currentMovie?.images?[indexPath.row].imagePath
            self.present(vc, animated: true, completion: nil)
            
        default:
            break
        }
    }
}

extension MovieDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    //MARK: Review Section
    
    func showAllComments() {
        
        guard let comments = currentMovie?.comments else {
            return
        }

        let vc = AllCommentsTableViewController.storyboardInit()
        vc.comments = comments
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK: Comments Table View delegate methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        commentTableViewBackground.isHidden = true
        
        guard let comments = currentMovie?.comments else {
            commentTableViewBackground.isHidden = false
            return 0
        }
        
        if comments.count == 0 {
            commentTableViewBackground.isHidden = false
        }
    
        if comments.count > 3 {
            
            reviewsSection.showAllButton.isHidden = false
            return 3
        }
        
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell", for: indexPath) as? CommentTableViewCell else {
            return UITableViewCell()
        }

        guard let comment = currentMovie?.comments?[indexPath.row] else {
            return cell
        }
        
        cell.nameLabel.text = comment.author
        cell.commentText.text = comment.content

        return cell
    }
}
