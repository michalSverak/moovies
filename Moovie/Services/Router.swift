//
//  Router.swift
//  Moovie
//
//  Created by Michal Sverak on 8/11/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//
//Source: Jindra Doležy @STRV

import Foundation
import Alamofire


protocol RouterRequest: URLRequestConvertible  {
    var method: HTTPMethod { get }
    var path: String { get }
    var params: [String: String]? { get }
}

extension RouterRequest {
    
    var baseURLString: String { return "https://api.themoviedb.org/3" }
    
    func asURLRequest() throws -> URLRequest {
        let url = try baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        if let parameters = params {
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        return urlRequest
    }
    
    var params: [String : String]? { return nil }
    var method: HTTPMethod { return .get }
}

struct Router {
    
    static let baseParams = ["api_key": AppSettings.apiKey,"language": AppSettings.apiLanguage]
    
    //MARK: Settings RouterRequest
    
    struct GetSettings: RouterRequest {
        
        var path: String { return "/configuration"}
        var params: [String : String]? { return baseParams }
    }
    
    //MARK: Movies RouterRequests
    
    struct GetMostPopularMovies: RouterRequest {
        
        let genres: String?
        var path: String { return "/discover/movie"}
        
        var params: [String : String]? {
            
            var params = baseParams
            
            params["sort_by"] = "popularity.desc"
            
            if genres != nil {
                params["with_genres"] = genres
                params["include_adult"] = AppSettings.includeAdultContent.description
            }
            
            params["include_image_language"] = AppSettings.apiImageLanguage
            
            return params
        }
    }
    
    struct GetPlayingNowMovies: RouterRequest {
        
        let today = Date().dateToString()
        let monthAgo = Calendar.current.date(byAdding: .month, value: -1, to: Date())?.dateToString()
        let genres: String?
        
        var path: String { return "discover/movie"}
        var params: [String : String]? {
            
            var params = baseParams
            
            params["primary_release_date.gte"] = monthAgo
            params["primary_release_date.lte"] = today
            params["include_adult"] = AppSettings.includeAdultContent.description
            
            if genres != nil {
                params["with_genres"] = genres
            }
            
            params["include_image_language"] = AppSettings.apiImageLanguage
            
            return params
        }
    }
    
    struct GetMovie: RouterRequest {
        let movieId: String

        var path: String { return "/movie/\(movieId)"}
        
        var params: [String : String]? {
            
            var params = baseParams
            
            params["include_image_language"] = AppSettings.apiImageLanguage
            params["append_to_response"] = "credits,images,videos,reviews"
            
            return params
        }
    }
    
    struct SearchMovies: RouterRequest {
        let queryString: String
        
        var path: String { return "/search/movie"}
        var params: [String : String]? {
            
            var params = baseParams
            params["query"] = queryString
            
            return params
        }
    }
    
    struct GetMovieReviews: RouterRequest {
        let movieId: String
        
        var path: String { return "/movie/\(movieId)/reviews"}
        var params: [String : String]? { return baseParams }
    }
    
    //MARK: Actors RouterRequests
    
    struct GetActor: RouterRequest {
        let actorId: String
        
        var path: String { return "/person/\(actorId)"}
        var params: [String : String]? {
            
            var params = baseParams
            params["append_to_response"] = "credits"
            
            return params
        }
    }
    
    struct GetPopularActors: RouterRequest {
        
        var path: String { return "/person/popular"}
        var params: [String : String]? { return baseParams }
    }
    
    struct SearchPeople: RouterRequest {
        let queryString: String
        
        var path: String { return "/search/person"}
        var params: [String : String]? {
            
            var params = baseParams
            params["query"] = queryString
            
            return params
        }
    }
    
    //MARK: User Profile RouterRequests
    
    struct requestToken: RouterRequest {
        
        var path: String { return "/authentication/token/new"}
        var params: [String : String]? { return ["api_key": AppSettings.apiKey] }
    }
    
    struct requestSessionID: RouterRequest {
        var token: String
        
        var path: String { return "/authentication/session/new"}
        var params: [String : String]? { return ["api_key": AppSettings.apiKey, "request_token": token] }
    }
    
    struct getAccount: RouterRequest {
        let sessionId: String
        
        var path: String { return "/account"}
        var params: [String : String]? { return ["api_key": AppSettings.apiKey, "session_id": sessionId] }
    }
    
    struct GetUsersFavoriteMovies: RouterRequest {
        let accountId: String
        let sessionId: String
        
        var path: String { return "/account/\(accountId)/favorite/movies"}
        var params: [String : String]? {
            
            var params = baseParams
            params["session_id"] = sessionId
            
            return params
        }
    }
    
    struct MarkAsFavorite: RouterRequest {
        let accountId: String
        let sessionId: String
        
        var method: HTTPMethod { return .post }
        var path: String { return "/account/\(accountId)/favorite"}
        
        var params: [String : String]? {
            
            var params = baseParams
            params["session_id"] = sessionId
            
            return params
        }
    }
    
    //MARK: Genres RouterRequests
    
    struct GetAllGenres: RouterRequest {
        
        var path: String { return "/genre/movie/list"}
        var params: [String : String]? { return baseParams }
    }
}
