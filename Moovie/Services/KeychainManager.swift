//
//  KeychainManager.swift
//  Moovie
//
//  Created by Michal Sverak on 9/8/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation
import KeychainSwift

protocol KeychainManager {
    
    func save(value: String?, for key: String)
    func get(for key: String) -> String?
    func clearKeychain()
    var sessionID: String? { get set }
    var accountID: String? { get set }
}

class MoovieKeychainManager: KeychainManager {
    
    let keychain: KeychainSwift
    
    init(keychain: KeychainSwift) {
        self.keychain = keychain
    }
    
    //MARK: Keychain manager methods
    
    func save(value: String?, for key: String) {
        
        guard let val = value else {
            print("Saved key cannot be nil")
            return
        }
        
        let result = keychain.set(val, forKey: key)
        
        if !result {
            //print("Something went wrong while saving key \(key): \(keychain.lastResultCode)")
        }
    }
    
    func get(for key: String) -> String? {
        
        if let pass = keychain.get(key) {
            return pass
        } else {
            //print("Something went wrong while retrieving key \(key): \(keychain.lastResultCode)")
        }
        return nil
    }
    
    func clearKeychain() {
        keychain.clear()
    }
}

extension MoovieKeychainManager {
    
    //MARK: Keychain manager key identifiers
    
    fileprivate struct Keys {
        static let seesionID = "sessionIDKey"
        static let accountID = "accountIDKey"
    }
}

extension MoovieKeychainManager {
    
    //MARK: Keychain manager keys
    
    var sessionID: String? {
        get { return get(for: Keys.seesionID) }
        set { save(value: newValue, for: Keys.seesionID)}
    }
    
    var accountID: String? {
        get { return get(for: Keys.accountID) }
        set { save(value: newValue, for: Keys.accountID)}
    }
}
