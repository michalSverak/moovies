//
//  DependencyInjectionContainer.swift
//  Moovie
//
//  Created by Michal Sverak on 9/4/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation
import Swinject
import KeychainSwift

struct DependencyInjectionContainer {
    
    static let assembler = Assembler([MoovieAssembly()])

    public static func resolve<T>(_ serviceType: T.Type) -> T? {
        
        return DependencyInjectionContainer.assembler.resolver.resolve(serviceType)
    }
}

class MoovieAssembly: Assembly {
    
    func assemble(container: Container) {
        
        //MARK: Services
        
        container.register(AlamofireAPIManager.self) { _ in
            
            return AlamofireAPIManager()
        }
        
        container.register(UserDefaultsStorage.self) { _ in
            
            return MoovieUserDefaultsStorage()
        }
        
        container.register(MovieSource.self) { resolver in
            
            return AlamofireMovieSource(apiManager: resolver.resolve(AlamofireAPIManager.self)!)
        }
        
        container.register(AuthenticationManager.self) { resolver in
            
            return MoovieAuthenticationManager(apiManager: resolver.resolve(AlamofireAPIManager.self)!)
        }
        
        container.register(KeychainManager.self) { _ in
            
            return MoovieKeychainManager(keychain: KeychainSwift())
        }
        
        container.register(UserDataSource.self) { resolver in
            
            return MoovieUserDataSource(apiManager: resolver.resolve(AlamofireAPIManager.self)!)
        }
        
        container.register(ActorSource.self) { resolver in
            
            return AlamofireActorSource()
        }
        
        //MARK: View Models
    
        container.register(MovieDetailViewModel.self) { resolver in
            
            return MovieDetailViewModel(movieSource: resolver.resolve(MovieSource.self)!, keychain: resolver.resolve(KeychainManager.self)!, userDefaultsStorage: resolver.resolve(UserDefaultsStorage.self)!)
        }
        
        container.register(MoviesViewModel.self) { resolver in
            
            return MoviesViewModel(movieSource: resolver.resolve(MovieSource.self)!, userDataSource: resolver.resolve(UserDataSource.self)!, userDefaultsStorage: resolver.resolve(UserDefaultsStorage.self)!, keychain: resolver.resolve(KeychainManager.self)!)
        }
        
        container.register(LoginViewModel.self) { resolver in
            
            return LoginViewModel(authenticationManager: resolver.resolve(AuthenticationManager.self)!, keychain: resolver.resolve(KeychainManager.self)!)
        }
        
        container.register(AuthenticationViewModel.self) { resolver in
            
            return AuthenticationViewModel(authenticationManager: resolver.resolve(AuthenticationManager.self)!, keychainManager: resolver.resolve(KeychainManager.self)!, userDataSource: resolver.resolve(UserDataSource.self)!, userDefaultsStorage: resolver.resolve(UserDefaultsStorage.self)!)
        }
        
        container.register(UserProfileViewModel.self) { resolver in
            return UserProfileViewModel(userDataSource: resolver.resolve(UserDataSource.self)!, keychainManager: resolver.resolve(KeychainManager.self)!, userDefaultsStorage: resolver.resolve(UserDefaultsStorage.self)!)
        }
        
        container.register(SearchViewModel.self) { resolver in
            
            return SearchViewModel(movieSource: resolver.resolve(MovieSource.self)!, userDefaultsStorage: resolver.resolve(UserDefaultsStorage.self)!)
        }
        
        container.register(ActorDetailViewModel.self) { resolver in
            
            return ActorDetailViewModel(actorSource: resolver.resolve(ActorSource.self)!)
        }
        
        container.register(ActorsViewModel.self) { resolver in
            
            return ActorsViewModel(actorSource: resolver.resolve(ActorSource.self)!)
        }
    }
}
