//
//  ViewModelState.swift
//  Moovie
//
//  Created by Michal Sverak on 8/26/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation

enum ViewModelState {
    
        case empty
        case loading
        case ready
        case error
}
