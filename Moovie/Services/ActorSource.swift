//
//  ActorSource.swift
//  Moovie
//
//  Created by Michal Sverak on 8/26/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation
import Alamofire
import UnboxedAlamofire
import Unbox

protocol ActorSource {
    
    func fetchPopular(completion: @escaping (APIResult<[PopularActor]>) -> Void)
    func fetchActorDetail(id: String, completion: @escaping (APIResult<Actor>) -> Void)
    func searchForActor(queryString: String, completion: @escaping (APIResult<[PopularActor]>) -> Void)
}

class AlamofireActorSource: ActorSource {
    
    func fetchPopular(completion: @escaping (APIResult<[PopularActor]>) -> Void) {
        Alamofire.request(Router.GetPopularActors()).validate().responseObject() { (result:DataResponse<APIActorResults>) in
            
            completion(result.asAPIResult() { $0.results } )
        }
    }
    
    func fetchActorDetail(id: String, completion: @escaping (APIResult<Actor>) -> Void) {
        Alamofire.request(Router.GetActor(actorId: id)).validate().responseObject() { (result:DataResponse<Actor>) in
            completion(result.asAPIResult())
        }
    }
    
    func searchForActor(queryString: String, completion: @escaping (APIResult<[PopularActor]>) -> Void) {
        Alamofire.request(Router.SearchPeople(queryString: queryString)).validate().responseObject() { (result:DataResponse<APIActorResults>) in
            
            completion(result.asAPIResult() { $0.results } )
        }
    }
}
