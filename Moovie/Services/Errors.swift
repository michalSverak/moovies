//
//  Errors.swift
//  Moovie
//
//  Created by Michal Sverak on 8/21/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation

enum MoovieError: Error {
    
    case genericError
    case invalidUrl
    case badObject
    case errorParsingJSON
    case errorUnboxingObject
    case invalidJson(Any)
    case missingSessionId
    case missingAccountId
    case markAsfavoriteFailure
}

extension MoovieError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .genericError:
            return NSLocalizedString("Something went wrong, sorry", comment: "General error")
        case .invalidUrl:
            return NSLocalizedString("There has been a request made to an invalid url, we are sorry.", comment: "Invalid Url")
        case .badObject, .errorParsingJSON, .errorUnboxingObject, .invalidJson:
            return NSLocalizedString("There has been an eror while retrieving data, sorry", comment: "Error retrieving data")
        case .missingAccountId, .missingSessionId:
            return NSLocalizedString("There was an error while authenticating user, sorry", comment: "Authentication error")
        case .markAsfavoriteFailure:
            return NSLocalizedString("There has been an issue with marking this movie as favorite. Please try again.", comment: "Mark as favorite error")
        }
    }
}
