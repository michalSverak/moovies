//
//  APIResults.swift
//  Moovie
//
//  Created by Michal Sverak on 8/26/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//
//Source: Jindra Doležy @STRV

import Foundation
import Alamofire

enum APIResult<T> {
    case success(T)
    case failure(Error)
    
    var value: T? {
        if case .success(let value) = self {
            return value
        } else {
            return nil
        }
    }
    
    var error: Error? {
        if case .failure(let error) = self {
            return error
        } else {
            return nil
        }
    }
}

extension DataResponse {
    
    func asAPIResult() -> APIResult<Value> {
        return asAPIResult() { $0 }
    }
    
    func asAPIResult<T>(mapBlock: (Value) -> T) -> APIResult<T> {
        switch self.result {
        case .success(let v):
            return .success(mapBlock(v))
        case .failure(let e):
            return .failure(e)
        }
    }
}
