//
//  ReachabilityManager.swift
//  Moovie
//
//  Created by Michal Sverak on 9/19/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation
import Alamofire

class ReachabilityManager {
    
    static let shared = ReachabilityManager()
    
    let observer = Alamofire.NetworkReachabilityManager(host: "www.google.com")
}
