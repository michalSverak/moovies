//
//  MovieSource.swift
//  Moovie
//
//  Created by Michal Sverak on 8/26/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation
import Alamofire
import UnboxedAlamofire
import Unbox

protocol MovieSource {
    
    func fetchMovieDetail(id: String, completion: @escaping (APIResult<Movie>) -> Void)
    func fetchNowPlaying(genres: String, completion: @escaping (APIResult<[FeaturedMovie]>) -> Void)
    func fetchMostPopular(genres: String, completion: @escaping (APIResult<[FeaturedMovie]>) -> Void)
    func markMovieAsFavorite(movieID: Int, sessionID: String, accountID: String, favorite: Bool, completion: @escaping (APIResult<PostedMovieStatus>)->Void)
    func searchForMovies(queryString: String, completion: @escaping (APIResult<[FeaturedMovie]>) -> Void)
}

class AlamofireMovieSource: MovieSource {
    
    let apiManager: APIManager
    
    init(apiManager: APIManager) {
        
        self.apiManager = apiManager
    }

    func fetchMostPopular(genres: String, completion: @escaping (APIResult<[FeaturedMovie]>) -> Void) {
        Alamofire.request(Router.GetMostPopularMovies(genres: genres)).validate().responseObject() { (result:DataResponse<APIMovieResults>) in

            completion(result.asAPIResult() { $0.results } )
        }
    }

    func fetchNowPlaying(genres: String, completion: @escaping (APIResult<[FeaturedMovie]>) -> Void) {
        Alamofire.request(Router.GetPlayingNowMovies(genres: genres)).validate().responseObject() { (result:DataResponse<APIMovieResults>) in
            completion(result.asAPIResult() { $0.results } )
        }
    }

    func fetchMovieDetail(id: String, completion: @escaping (APIResult<Movie>) -> Void) {
    
        apiManager.request(withRoute: Router.GetMovie(movieId: id)) { result in
            
            completion(result)
        }
    }

    func markMovieAsFavorite(movieID: Int, sessionID: String, accountID: String, favorite: Bool, completion: @escaping (APIResult<PostedMovieStatus>)->Void) {
        
        let url = AppSettings.baseUrl + "/account/\(accountID)/favorite?api_key=\(AppSettings.apiKey)&session_id=\(sessionID)"

        let movieInfo: [String : Any] = [
            "media_type":"movie",
            "media_id": movieID,
            "favorite": favorite
        ]
        
        let headers = [ "Accept":"application/json", "charset":"utf-8"]
        
        Alamofire.request(url, method: .post, parameters: movieInfo, encoding: JSONEncoding.default, headers: headers).validate().responseJSON {  response in
            print(response)
            
            switch response.result {
            case .success(let dict as UnboxableDictionary):
                do {
                    try completion(.success(unbox(dictionary: dict)))
                } catch {
                    completion(.failure(error))
                }
            case .success(let dict):
                completion(.failure(MoovieError.invalidJson(dict)))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func searchForMovies(queryString: String, completion: @escaping (APIResult<[FeaturedMovie]>) -> Void) {
        Alamofire.request(Router.SearchMovies(queryString: queryString)).validate().responseObject() { (result:DataResponse<APIMovieResults>) in
            
            completion(result.asAPIResult() { $0.results } )
        }
    }
}
    
