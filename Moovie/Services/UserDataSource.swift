//
//  UserDataSource.swift
//  Moovie
//
//  Created by Michal Sverak on 9/8/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation
import Alamofire
import UnboxedAlamofire
import Unbox

protocol UserDataSource {
    func fetchUserDetail(sessionId: String, completion: @escaping (APIResult<User>) -> Void)
    func fetchUserfavoriteMovies(sessionID: String, accountID: String, completion: @escaping (APIResult<[FeaturedMovie]>) -> Void)
}

class MoovieUserDataSource: UserDataSource {
    
    let apiManager: AlamofireAPIManager
    
    init(apiManager: AlamofireAPIManager) {
        
        self.apiManager = apiManager
    }
    
    func fetchUserDetail(sessionId: String, completion: @escaping (APIResult<User>) -> Void) {
        
        apiManager.request(withRoute: Router.getAccount(sessionId: sessionId)) { result in
            
            completion(result)
        }
    }
    
    func fetchUserfavoriteMovies(sessionID: String, accountID: String, completion: @escaping (APIResult<[FeaturedMovie]>) -> Void) {
        Alamofire.request(Router.GetUsersFavoriteMovies(accountId: accountID, sessionId: sessionID)).validate().responseObject() { (result:DataResponse<APIMovieResults>) in
            
            completion(result.asAPIResult() { $0.results } )
        }
    }
}
