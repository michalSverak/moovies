//
//  MockAPIManager.swift
//  Moovie
//
//  Created by Michal Sverak on 9/15/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//
// Source: Jan Kaltoun @STRV

import Foundation
import Alamofire
import Unbox

// MockAPIManager is used for unit testing

class MockAPIManager: APIManager {
    
    func request<T: Unboxable>(withRoute: URLRequestConvertible, completion: @escaping (APIResult<T>) -> Void) {
        
        guard
            let request = try? withRoute.asURLRequest(),
            let httpMethod = request.httpMethod,
            let url = request.url
            else {
                
                fatalError("Something is horribly wrong")
        }
        
        var jsonFilename: String? = nil
        
        if httpMethod == "GET" && url.relativePath == "/3/movie/123" {
            
            jsonFilename = "mockMovieData"
        }
        
        let bundle = Bundle(for: type(of: self))
        
        guard
            let file = bundle.url(forResource: jsonFilename, withExtension: "json"),
            let data = try? Data(contentsOf: file),
            let unboxed: T = try? unbox(data: data)
            else {
                
                fatalError("Something else went horribly wrong.")
        }
        
        let result = APIResult.success(unboxed)
        
        completion(result)
    }
}
