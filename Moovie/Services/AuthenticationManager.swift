//
//  AuthenticationManager.swift
//  Moovie
//
//  Created by Michal Sverak on 9/7/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation
import Alamofire
import Unbox

protocol AuthenticationManager {
    
    func fetchNewToken(completion: @escaping (APIResult<Token>) -> Void)
    func fetchSessionID(token: String, completion: @escaping (APIResult<SessionID>) -> Void)
    func fetchAccountID(sessionId: String, completion: @escaping (APIResult<User>) -> Void)
}

class MoovieAuthenticationManager: AuthenticationManager {
    
    let apiManager: AlamofireAPIManager
    
    init(apiManager: AlamofireAPIManager) {
        
        self.apiManager = apiManager
    }
    
    func fetchNewToken(completion: @escaping (APIResult<Token>) -> Void) {
        
        Alamofire.request(Router.requestToken()).validate().responseObject() { (result:DataResponse<Token>) in
            
            completion(result.asAPIResult())
        }
    }
    
    func fetchSessionID(token: String, completion: @escaping (APIResult<SessionID>) -> Void) {
        
        Alamofire.request(Router.requestSessionID(token: token)).validate().responseObject() { (result:DataResponse<SessionID>) in
            
            completion(result.asAPIResult())
        }
    }
    
    func fetchAccountID(sessionId: String, completion: @escaping (APIResult<User>) -> Void) {
        
        Alamofire.request(Router.getAccount(sessionId: sessionId)).validate().responseObject() { (result:DataResponse<User>) in
            
            completion(result.asAPIResult())
        }
    }
}
