//
//  GenreSource.swift
//  Moovie
//
//  Created by Michal Sverak on 8/26/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//
//Source: Jindra Doležy @STRV

import Foundation
import Alamofire
import Unbox

protocol GenreSource {
    func fetchGenres(completion: @escaping (APIResult<[MovieGenre]>)->Void)
}

class AlamofireGenreSource: GenreSource {
    
    func fetchGenres(completion: @escaping (APIResult<[MovieGenre]>)->Void) {
        Alamofire.request(Router.GetAllGenres()).validate().responseJSON() { response in
            switch response.result {
            case .success(let dict as UnboxableDictionary):
                do {
                    try completion(.success(unbox(dictionary: dict, atKey: "genres")))
                } catch {
                    completion(.failure(error))
                }
            case .success(let dict):
                completion(.failure(MoovieError.invalidJson(dict)))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
