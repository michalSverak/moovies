//
//  ImageSizeManager.swift
//  Moovie
//
//  Created by Michal Sverak on 9/16/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation

enum ImageType {
    
    case poster
    case backdrop
    case profile
    case original
}

class ImageSizeManager {
    
    static var shared: ImageSizeManager = {
        let manager = ImageSizeManager(imageSource: MoovieImageSettingsSource())
        manager.reload()
        return manager
    }()
    
    let imageSource: ImageSettingsSource
    var loading = false
    var imageSizes: ImageSizes?
    let originalSize = "original"
    
    init(imageSource: ImageSettingsSource) {
        
        self.imageSource = imageSource
    }
    
    fileprivate func reload() {
        
        if loading {
            return
        }
        
        loading = true
        
        imageSource.fetchImageSizes { result in
            
            if let imageSizes = result.value {
                self.imageSizes = imageSizes
            }
            
            if let error = result.error {
                print(error)
            }
            
            self.loading = false
        }
    }
    
    func baseImageUrl(imagesize: Int, type: ImageType) -> String? {
        
        guard let size = self.imageSizes else {
            return nil
        }
        
        let urlString = size.secureBaseUrl + imageSize(imagesize, forType: type)
        
        return urlString
    }
    
    fileprivate func imageSize(_ size: Int, forType: ImageType) -> String {
        
        // Find closest value to given size. If something fails return "original" which represents original image size
        
        guard let imageSizes = self.imageSizes else {
            return originalSize
        }
        
        var sizes = [String]()
        
        // Select right array of sizes
        
        switch forType {
        case .backdrop:
            sizes = imageSizes.backdropSizes
        case .poster:
            sizes = imageSizes.posterSizes
        case .profile:
            sizes = imageSizes.profileSizes
        case .original:
            return originalSize
        }
        
        // Remove value "original" and map array to Integers
        sizes = sizes.filter{ $0 != originalSize }
        sizes = sizes.map { String($0.characters.dropFirst(1)) }
        var intSizes = sizes.map { Int($0) ?? 0 }
        intSizes = intSizes.sorted { $1 > $0 }
        
        // Find closest value to given size
        let closestValue = intSizes.enumerated().min (by: { abs($0.1 - size) < abs($1.1 - size) })
        
        // If closest value is more than 50px smaller than given size return next available image size instead to ensure good image quality
        
        if let value = closestValue, value.element <= (size - 50) {
            
            guard (value.offset + 1) < intSizes.count else {
                return originalSize
            }
            
            let nextItem = intSizes[value.offset + 1]
            
            return "w" + String(nextItem)
            
        }
        
        // If requested image size is greater than largest available image size return "original"
        if let last = intSizes.last, size > last {
            return originalSize
        }
        
        if let value = closestValue?.element {
            return "w" + String(value)
        }
        
        return originalSize
    }
}
