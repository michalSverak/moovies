//
//  ImageSettingsSource.swift
//  Moovie
//
//  Created by Michal Sverak on 9/16/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation
import Alamofire
import Unbox

protocol ImageSettingsSource {
    
    func fetchImageSizes(_ completion: @escaping (APIResult<ImageSizes>) -> Void)
}

class MoovieImageSettingsSource: ImageSettingsSource {
    
    func fetchImageSizes(_ completion: @escaping (APIResult<ImageSizes>) -> Void) {
        Alamofire.request(Router.GetSettings()).validate().responseJSON() { response in
            switch response.result {
            case .success(let dict as UnboxableDictionary):
                do {
                    try completion(.success(unbox(dictionary: dict, atKey: "images")))
                } catch {
                    completion(.failure(error))
                }
            case .success(let dict):
                completion(.failure(MoovieError.invalidJson(dict)))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
