//
//  AlamofireAPIManager.swift
//  Moovie
//
//  Created by Michal Sverak on 8/31/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//
// Source: Honza Kaltou @ STRV

import Foundation
import Alamofire
import UnboxedAlamofire
import Unbox

// APIManager wraps Alamofire request in order to allow unit testing of requests

protocol APIManager: class {
    
    func request(withRoute: URLRequestConvertible, completion: @escaping (APIResult<Movie>) -> Void)
}

class AlamofireAPIManager: APIManager {
    
    let sessionManager = SessionManager()
    
    func request<T: Unboxable>(withRoute: URLRequestConvertible, completion: @escaping (APIResult<T>) -> Void) {
        
        sessionManager.request(withRoute).validate().responseObject { (result:DataResponse<T>) in
            
            completion(result.asAPIResult())
        }
    }
}
