//
//  GenreManager.swift
//  Moovie
//
//  Created by Michal Sverak on 8/26/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//
//Source: Jindra Doležy @STRV

import Foundation

class GenreManager {
    static var shared: GenreManager = {
        let manager = GenreManager(source: AlamofireGenreSource())
        manager.reload()
        return manager
    }()
    
    let source: GenreSource
    var genres: [Int:MovieGenre]?
    var genresArray = [MovieGenre]()
    var loading: Bool = false
    
    static var defaultGenres: [Int:MovieGenre] = {
        return [:]
    }()
    
    init(source: GenreSource) {
        self.source = source
    }
    
    func reload() {
        if loading {
            return
        }
        loading = true
        
        source.fetchGenres() { result in
            if let genres = result.value {
                
                self.genresArray = genres
                
                var genreMap = [Int:MovieGenre]()
                genres.forEach {
                    genreMap[$0.id] = $0
                }
                self.genres = genreMap
            }
            if let error = result.error {
                print(error)
            }
            self.loading = false
        }
    }
    
    func map(id: Int) -> MovieGenre? {
        if let genres = genres {
            return genres[id]
        } else {
            reload()
            return GenreManager.defaultGenres[id]
        }
    }
    
    func map(ids: [Int]) -> [MovieGenre] {
        return ids.flatMap { self.map(id: $0) }
    }
}
