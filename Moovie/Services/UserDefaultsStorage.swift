//
//  UserDefaultsStorage.swift
//  Moovie
//
//  Created by Michal Sverak on 8/25/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation

protocol UserDefaultsStorage {
    
    func save(value: Any?, for key: String)
    func getValue(for key: String) -> Any?
    func deleteAllUserDefaults()
    var favoriteMovies: [Int]? { get set }
    var lastSearchedMovie: String? { get set }
    var lastSearchedActor: String? { get set }
}

struct MoovieUserDefaultsStorage: UserDefaultsStorage {
    
    //MARK: User default methods
    
    private let userDefaults = UserDefaults.standard
    
    func save(value: Any?, for key: String) {
        userDefaults.setValue(value, forKey: key)
        userDefaults.synchronize()
    }
    
    func getValue(for key: String) -> Any? {
        return userDefaults.object(forKey: key)
    }
    
    func deleteAllUserDefaults() {
        
        if let bundle = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: bundle)
        }
    }
}

extension MoovieUserDefaultsStorage {
    
    //MARK: User default storage keys
    
    struct Keys {
        static let favoriteMovies = "favoriteMovies"
        static let lastSearchedMovie = "lastSearchedMovie"
        static let lastSearchedActor = "lastSearchedActor"
    }
}

extension MoovieUserDefaultsStorage {
    
    //MARK: User default stored data
    
    var favoriteMovies: [Int]? {
        get { return getValue(for: Keys.favoriteMovies) as? [Int] }
        set { save(value: newValue, for: Keys.favoriteMovies) }
    }
    
    var lastSearchedMovie: String? {
        get { return getValue(for: Keys.lastSearchedMovie) as? String }
        set { save(value: newValue, for: Keys.lastSearchedMovie) }
    }
    
    var lastSearchedActor: String? {
        get { return getValue(for: Keys.lastSearchedActor) as? String }
        set { save(value: newValue, for: Keys.lastSearchedActor) }
    }
}
