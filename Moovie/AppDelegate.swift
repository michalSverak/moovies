//
//  AppDelegate.swift
//  Moovie
//
//  Created by Michal Sverak on 7/27/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import UIKit
import SVProgressHUD
import AlamofireNetworkActivityIndicator

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Initial Navigation Controller
        window = UIWindow(frame: UIScreen.main.bounds)
        let mainController = TabBarController()
        self.window?.rootViewController = mainController
        self.window?.makeKeyAndVisible()
        
        // Setup navigation bar across whole app
        _ = SetupNavigationBar().setup()
        
        // Network activity indicator
        NetworkActivityIndicatorManager.shared.isEnabled = true
        
        // Preload genres
        _ = GenreManager.shared
        
        // Preload Image settings
        _ = ImageSizeManager.shared
        
        // Set global tint
        self.window?.tintColor = ColorPalette.STRVred
        
        SVProgressHUD.setDefaultStyle(.dark)
        
        return true
    }
}
