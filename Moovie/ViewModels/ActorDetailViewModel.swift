//
//  ActorDetailViewModel.swift
//  Moovie
//
//  Created by Michal Sverak on 8/9/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation

protocol ActorDetailViewModelDelegate: class {
    func dataUpdated()
    func handleError(_ error: Error)
}

class ActorDetailViewModel {
    
    var actorDetail: ActorStub?
    weak var delegate: ActorDetailViewModelDelegate?
    let actorSource: ActorSource
    var error: Error?
    var actorId: String?
    
    var state: ViewModelState = .empty {
        didSet {
            if state != oldValue {
                delegate?.dataUpdated()
            }
        }
    }
    
    //MARK: ViewModel init + Dependency injection
    
    static func resolve() -> ActorDetailViewModel {
        
        return DependencyInjectionContainer.resolve(ActorDetailViewModel.self)!
    }

    init(actorSource: ActorSource) {
        self.actorSource = actorSource
    }
    
    //MARK: ViewModel methods
    
    func getActorDetail(id: String) {
        
        if state == .loading {
            return
        }
        
        state = .loading
        
        self.actorId = id
        
        actorSource.fetchActorDetail(id: id) { [weak self] result in
            guard let `self` = self else {
                return
            }
            
            let df = DateFormatter()
            
            if let value = result.value {
                
                let movies = value.movies.map {
                    
                    FeaturedMovieStub(
                        movieId: $0.movieId,
                        genres: [],
                        posterPath: $0.posterPath ?? "",
                        originalTitle: $0.originalTitle,
                        releaseDate: $0.releaseDate?.yearToString(),
                        imagePath: $0.imagePath ?? "",
                        overview: $0.overview ?? "",
                        rating: $0.voteAverage ?? 0,
                        character: $0.character ?? "")
                }
                
                let gender = { () -> String in 
                    switch result.value!.gender {
                    case 1:
                        return "ACTRESS"
                    case 2:
                        return "ACTOR"
                    default:
                        return ""
                    }
                }()
                
                df.timeStyle = .none
                df.dateStyle = .medium
                
                var birthday = ""
                
                if let brd = value.birthday {
                    birthday = df.string(from: brd)
                }
                
               self.actorDetail =  ActorStub(
                    actorId: value.actorId,
                    gender: gender,
                    name: value.name,
                    bio: value.bio,
                    birthday: birthday,
                    placeOfBirth: value.placeOfBirth ?? "",
                    imagePath: value.imagePath ?? "",
                    movies: movies.sorted {
                        guard let date0 = $0.releaseDate, let date1 = $1.releaseDate else {
                            return false
                        }
                        return date0 > date1
               })
                
                self.state = .ready
                
            } else {
                
                guard let error = result.error else {
                    self.delegate?.handleError(MoovieError.genericError)
                    return
                }
                
                self.error = result.error
                self.state = .error
                self.delegate?.handleError(error)
            }
        }
    }
}
