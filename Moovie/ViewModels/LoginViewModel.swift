//
//  LoginViewModel .swift
//  Moovie
//
//  Created by Michal Sverak on 9/7/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation

protocol LoginViewViewModelProtocol: class {
    
    func tokenRecieved()
    func handleError(_ error: Error)
}

class LoginViewModel {

    weak var delegate: LoginViewViewModelProtocol?
    var token: Token?
    var error: Error?
    var authenticationManager: AuthenticationManager
    var state: ViewModelState = .empty
    var keychain: KeychainManager
    
    //MARK: ViewModel init + Dependency injection
    
    static func resolve() -> LoginViewModel? {
        
        return DependencyInjectionContainer.resolve(LoginViewModel.self)
    }
    
    init(authenticationManager: AuthenticationManager, keychain: KeychainManager) {
        
        self.authenticationManager = authenticationManager
        self.keychain = keychain
    }
    
    //MARK: ViewModel methods
    
    func isUserLoggedIn() -> Bool {
       
        guard keychain.sessionID != nil, keychain.accountID != nil else {
            return false
        }

        return true
    }
    
    func requestToken() {
        
        if state == .loading {
            return
        }
        state = .loading
    
        authenticationManager.fetchNewToken { [weak self] result in
            
            guard let `self` = self else {
                return
            }
            
            if let value = result.value {
                self.token = value
                self.delegate?.tokenRecieved()
                self.state = .ready
                
            } else {
                
                if let error = result.error {
                    self.error = error
                    self.state = .empty
                    self.delegate?.handleError(error)
                }
            }
        }
    }
}
