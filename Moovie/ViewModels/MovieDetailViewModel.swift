//
//  MovieDetailViewModel.swift
//  Moovie
//
//  Created by Michal Sverak on 8/4/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation

protocol MovieDetailViewModelDelegate: class {
    func dataUpdated()
    func handleError(_ error: Error)
    func updateFavorite()
}

class MovieDetailViewModel {
    
    var movieId: Int?
    var movieDetail: Movie?
    weak var delegate: MovieDetailViewModelDelegate?
    
    let movieSource: MovieSource
    let keychain: KeychainManager
    var userDefaultsStorage: UserDefaultsStorage
    var error: Error?
    
    var state: ViewModelState = .empty {
        didSet {
            if state != oldValue {
                delegate?.dataUpdated()
            }
        }
    }
    
    //MARK: ViewModel init + Dependency injection

    static func resolve() -> MovieDetailViewModel? {
        
        return DependencyInjectionContainer.resolve(MovieDetailViewModel.self)
    }
    
    init(movieSource: MovieSource, keychain: KeychainManager, userDefaultsStorage: UserDefaultsStorage) {
        
        self.movieSource = movieSource
        self.keychain = keychain
        self.userDefaultsStorage = userDefaultsStorage
    }
    
    //MARK: ViewModel methods
    
    func isUserLoggedIn() -> Bool {
        
        guard keychain.sessionID != nil, keychain.accountID != nil else {
            return false
        }
        
        return true
    }
    
    func getData() {
        
        if state == .loading {
            return
        }
        state = .loading
        
        guard let id = movieId else {
            delegate?.handleError(MoovieError.genericError)
            return
        }
        
        getMovieDetail(id: String(id))
    }
    
    func isMovieFavorite() -> Bool {
        
        guard let favorites = userDefaultsStorage.favoriteMovies else {
            return false
        }
        
        guard let id = movieId else {
            return false
        }
        
        if favorites.contains(id) {
            return true
        }
        
        return false
    }
    
    func updateFavorites(favorite: Bool, movieID: Int) {
        
        guard favorite == true else {
            
            //If favorite = false remove movie id from favorites in user defaults
            self.userDefaultsStorage.favoriteMovies = self.userDefaultsStorage.favoriteMovies?.filter() {
                $0 != movieID
            }
            
            return
        }
        
        //If favorite = true append id to favorites in user defaults
        self.userDefaultsStorage.favoriteMovies?.append(movieID)
    }
    
    func getMovieDetail(id: String) {
        
        movieSource.fetchMovieDetail(id: id) { [weak self] result in
            
            guard let `self` = self else {
                return
            }
            
            if let value = result.value {
                self.movieDetail = value
                self.delegate?.dataUpdated()
                
                self.state = .ready
                
            } else {
                
                guard let error = result.error else {
                    self.delegate?.handleError(MoovieError.genericError)
                    return
                }
                
                self.error = result.error
                self.state = .error
                self.delegate?.handleError(error)
            }
        }
    }
    
    func markAsFavorite(movieID: Int, favorite: Bool) {
    
            guard let sessionID = keychain.sessionID else {
    
                error = MoovieError.missingSessionId
                state = .error
                delegate?.handleError(MoovieError.missingSessionId)
                return
            }
    
            guard let accountID = keychain.accountID else {
    
                error = MoovieError.missingAccountId
                state = .error
                delegate?.handleError(MoovieError.missingAccountId)
                return
            }
    
        movieSource.markMovieAsFavorite(movieID: movieID, sessionID: sessionID, accountID: accountID, favorite: favorite) { [weak self] result in
            
            guard let `self` = self else {
                return
            }
            
            if let value = result.value {
                
                /*
                 ids:
                 1 created succesfully in user favorites
                 12 updated succesfully
                 13 deleted succesfully from user favorites
                 */
                
                if value.statusCode == 1 || value.statusCode == 12 || value.statusCode == 13 {
                    
                    //update favorites in user defaults
                    self.updateFavorites(favorite: favorite, movieID: movieID)
    
                    //Update favorite icon
                    self.delegate?.updateFavorite()
                    
                    return
                }
                self.error = MoovieError.markAsfavoriteFailure
                self.state = .error
                self.delegate?.handleError(MoovieError.markAsfavoriteFailure)
                
            } else {
                
                guard let error = result.error else {
                    self.delegate?.handleError(MoovieError.genericError)
                    return
                }
                
                self.error = result.error
                self.state = .error
                self.delegate?.handleError(error)
            }
        }
    }
}
