//
//  ActorsViewModel.swift
//  Moovie
//
//  Created by Michal Sverak on 8/9/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation

protocol ActorsViewModelDelegate: class {
    func dataDownloaded()
    func handleError(_ error: Error)
}

class ActorsViewModel {
    
    var popularActors = [PopularActor]()
    let actorSource: ActorSource
    weak var delegate: ActorsViewModelDelegate?
    var error: Error?

    var state: ViewModelState = .empty
    
    //MARK: ViewModel init + Dependency injection
    
    static func resolve() -> ActorsViewModel {
        
        return DependencyInjectionContainer.resolve(ActorsViewModel.self)!
    }
    
    init(actorSource: ActorSource) {
        self.actorSource = actorSource
    }
    
    //MARK: ViewModel methods

    func getMostPopularActors() {
        
        if state == .loading {
            return
        }
        state = .loading
        
        actorSource.fetchPopular { [weak self] result in
            
            guard let `self` = self else {
                return
            }
            
            if let value = result.value {
                self.popularActors = value
                self.delegate?.dataDownloaded()
                
                self.state = self.popularActors.isEmpty ? .empty : .ready
                
            } else {
                
                guard let error = result.error else {
                    self.delegate?.handleError(MoovieError.genericError)
                    return
                }
                
                self.error = result.error
                self.state = .error
                self.delegate?.handleError(error)
            }
        }
    }
}
