//
//  UserProfileViewModel.swift
//  Moovie
//
//  Created by Michal Sverak on 8/9/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation

protocol UserProfileViewModelDelegate: class {
    
    func updateView()
    func handleError(_ error: Error)
    func userFavoriteMoviesReady()
}

class UserProfileViewModel {

    weak var delegate: UserProfileViewModelDelegate?
    let userDataSource: UserDataSource
    var keychain: KeychainManager
    var error: Error?
    var state: ViewModelState = .empty
    var user: User?
    var userFavoriteMovies = [FeaturedMovieStub]()
    var userDefaultsStorage: UserDefaultsStorage
    
    //MARK: ViewModel init + Dependency injection
    
    init(userDataSource: UserDataSource, keychainManager: KeychainManager, userDefaultsStorage: UserDefaultsStorage) {
        
        self.userDataSource = userDataSource
        self.keychain = keychainManager
        self.userDefaultsStorage = userDefaultsStorage
    }
    
    static func resolve() -> UserProfileViewModel? {
        
        return DependencyInjectionContainer.resolve(UserProfileViewModel.self)
    }
    
    //MARK: ViewModel methods
    
    func isUserLoggedIn() -> Bool {
        
        guard keychain.sessionID != nil, keychain.accountID != nil else {
            return false
        }
        
        return true
    }
    
    func getUser() {

        if state == .loading {
            return
        }
        state = .loading
        
        guard let sessionID = keychain.sessionID else {
            
            error = MoovieError.missingSessionId
            state = .empty
            delegate?.handleError(MoovieError.missingSessionId)
            return
        }
        
        userDataSource.fetchUserDetail(sessionId: sessionID) {  [weak self] result in
            
            guard let `self` = self else {
                return
            }
            
            if let value = result.value {
                self.user = value
                self.keychain.accountID = String(value.id)
                self.state = .ready
                self.delegate?.updateView()
                
            } else {
                
                if let error = result.error {
                    self.error = error
                    self.state = .empty
                    self.delegate?.handleError(error)
                }
            }
        }
    }
    
    func getUserFavoriteMovies() {
        
        guard let sessionID = keychain.sessionID else {
            
            error = MoovieError.missingSessionId
            state = .error
            delegate?.handleError(MoovieError.missingSessionId)
            return
        }
        
        guard let accountID = keychain.accountID else {
            
            error = MoovieError.missingAccountId
            state = .error
            delegate?.handleError(MoovieError.missingAccountId)
            return
        }
        
        userDataSource.fetchUserfavoriteMovies(sessionID: sessionID, accountID: accountID ) {  [weak self] result in
            
            guard let `self` = self else {
                return
            }
            
            if let value = result.value {
                
                self.userFavoriteMovies = value.map {
                    
                    FeaturedMovieStub(
                        movieId: $0.movieId,
                        genres: GenreManager.shared.map(ids: $0.genreIds!).map { $0.name },
                        posterPath: $0.posterPath ?? "",
                        originalTitle: $0.originalTitle,
                        releaseDate: nil, imagePath: $0.imagePath ?? "",
                        overview: $0.overview ?? "",
                        rating: $0.voteAverage ?? 0,
                        character: "")
                }
                
                var movieIDs = [Int]()
                for id in value {
                    movieIDs.append(id.movieId)
                }
                self.userDefaultsStorage.favoriteMovies = movieIDs
                self.delegate?.userFavoriteMoviesReady()
                self.state = self.userFavoriteMovies.isEmpty ? .empty : .ready

            } else {
                
                guard let error = result.error else {
                    return
                }
                
                self.error = error
                self.state = .error
                self.delegate?.handleError(error)
            }
        }
    }
 }
