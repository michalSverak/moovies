//
//  AuthenticationViewModel.swift
//  Moovie
//
//  Created by Michal Sverak on 9/7/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation

protocol AuthenticationViewModelProtocol: class {
    func userIsAuthenticated()
    func handleError(_ error: Error)
}

class AuthenticationViewModel {
    
    weak var delegate: AuthenticationViewModelProtocol?
    var error: Error?
    var authenticationManager: AuthenticationManager
    var state: ViewModelState = .empty
    var keychain: KeychainManager
    let userDataSource: UserDataSource
    var userDefaultsStorage: UserDefaultsStorage
    
    //MARK: ViewModel init + Dependency injection
    
    static func resolve() -> AuthenticationViewModel? {
        
        return DependencyInjectionContainer.resolve(AuthenticationViewModel.self)
    }
    
    init(authenticationManager: AuthenticationManager, keychainManager: KeychainManager, userDataSource: UserDataSource, userDefaultsStorage: UserDefaultsStorage) {
        
        self.authenticationManager = authenticationManager
        self.keychain = keychainManager
        self.userDataSource = userDataSource
        self.userDefaultsStorage = userDefaultsStorage
    }
    
    //MARK: ViewModel methods
    
    func requestSessionID(token: String) {
        
        if state == .loading {
            return
        }
        state = .loading
        
        authenticationManager.fetchSessionID(token: token) { [weak self] result in
            
            guard let `self` = self else {
                return
            }
            
            if let value = result.value {
                self.keychain.sessionID = value.sessionID
                self.state = .ready
                self.requestAccountID(sessionID: value.sessionID)
                
            } else {
                
                if let error = result.error {
                    self.error = error
                    self.state = .empty
                    self.delegate?.handleError(error)
                }
            }
        }
    }
    
    func requestAccountID(sessionID: String) {
        
        if state == .loading {
            return
        }
        state = .loading
        
        authenticationManager.fetchAccountID(sessionId: sessionID) { [weak self] result in
            guard let `self` = self else {
                return
            }
            
            if let value = result.value {
                self.keychain.accountID = String(value.id)
                self.getUserFavoriteMovies()
                self.delegate?.userIsAuthenticated()
                self.state = .ready
                
            } else {
                
                if let error = result.error {
                    self.error = error
                    self.state = .empty
                    self.delegate?.handleError(error)
                }
            }
        }
    }
    
    func getUserFavoriteMovies() {
        
        guard let sessionID = keychain.sessionID else {
            
            error = MoovieError.missingSessionId
            state = .error
            delegate?.handleError(MoovieError.missingSessionId)
            return
        }
        
        guard let accountID = keychain.accountID else {
            
            error = MoovieError.missingAccountId
            state = .error
            delegate?.handleError(MoovieError.missingAccountId)
            return
        }
        
        userDataSource.fetchUserfavoriteMovies(sessionID: sessionID, accountID: accountID ) {  [weak self] result in
            
            guard let `self` = self else {
                return
            }
            
            if let value = result.value {
                
                var movieIDs = [Int]()
                for id in value {
                    movieIDs.append(id.movieId)
                }
                self.userDefaultsStorage.favoriteMovies = movieIDs
                
            } else {
                
                guard let error = result.error else {
                    self.delegate?.handleError(MoovieError.genericError)
                    return
                }
                
                self.error = result.error
                self.state = .error
                self.delegate?.handleError(error)
            }
        }
    }
}
