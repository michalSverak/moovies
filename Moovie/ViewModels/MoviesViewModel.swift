//
//  MoviesViewModel.swift
//  Moovie
//
//  Created by Michal Sverak on 8/3/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation

protocol MoviesViewModelDelegate: class {
    
    func dataDownloaded()
    func handleError(_ error: Error)
}

class MoviesViewModel {
    
    var mostPopularMovies = [FeaturedMovieStub]()
    var nowPlayingMovies = [FeaturedMovieStub]()
    weak var delegate: MoviesViewModelDelegate?
    let movieSource: MovieSource
    var keychain: KeychainManager
    var error: Error?
    var selectedGenres = [MovieGenre]()
    var userDefaultsStorage: UserDefaultsStorage
    let userDataSource: UserDataSource
    var selectedGenresStringIds = "" {
        didSet {
            getData()
        }
    }
    
    var state: ViewModelState = .empty {
        didSet {
            if state != oldValue {
                delegate?.dataDownloaded()
            }
        }
    }
    
   //MARK: ViewModel init + Dependency injection
    
    static func resolve() -> MoviesViewModel {
        
        return DependencyInjectionContainer.resolve(MoviesViewModel.self)!
    }

    init(movieSource: MovieSource, userDataSource: UserDataSource, userDefaultsStorage: UserDefaultsStorage, keychain: KeychainManager ) {
        
        self.movieSource = movieSource
        self.userDataSource = userDataSource
        self.userDefaultsStorage = userDefaultsStorage
        self.keychain = keychain
    }
    
    //MARK: ViewModel methods
    
    func isUserLoggedIn() -> Bool {
        
        guard keychain.sessionID != nil, keychain.accountID != nil else {
            return false
        }
        
        return true
    }
    
    func getData() {
    
        if state == .loading {
            return
        }
        state = .loading
        
        getMostPopularMovies()
        getPlayingNowMovies()
        
        guard isUserLoggedIn() else {
            return
        }
        
        getUserFavoriteMovies()
    }
    
    func setGenres(genres: [MovieGenre]) {
        
        //Store genres selected in filter view in viewModel
        self.selectedGenres = genres
        
        //Create string representation of all genre ids
        var genreIds = [Int]()
        for g in selectedGenres {
            genreIds.append(g.id)
        }
        
        var genresString = ""
        
        for genre in genreIds {
            
            genresString += "\(genre)" + ","
        }
    
        selectedGenresStringIds = String(genresString.characters.dropLast(1))
    }

    func getPlayingNowMovies() {
        
        self.nowPlayingMovies = [FeaturedMovieStub]()
        self.state = .empty

        movieSource.fetchNowPlaying(genres: selectedGenresStringIds) { [weak self]result in
            
            guard let `self` = self else {
                return
            }
            
            if let value = result.value {

                self.nowPlayingMovies = value.map {
                    
                    FeaturedMovieStub(
                        movieId: $0.movieId,
                        genres: GenreManager.shared.map(ids: $0.genreIds!).map { $0.name },
                        posterPath: $0.posterPath ?? "",
                        originalTitle: $0.originalTitle,
                        releaseDate: nil, imagePath: $0.imagePath ?? "",
                        overview: $0.overview ?? "",
                        rating: $0.voteAverage ?? 0, character: "")
                }
                
                self.delegate?.dataDownloaded()
                self.state = self.nowPlayingMovies.isEmpty ? .empty : .ready
                
            } else {
                
                if self.state == .error {
                    return
                }
                
                guard let error = result.error else {
                    self.delegate?.handleError(MoovieError.genericError)
                    return
                }
                
                self.error = result.error
                self.state = .error
                self.delegate?.handleError(error)
            }
        }
    }
    
    func getMostPopularMovies() {
        
        self.mostPopularMovies = [FeaturedMovieStub]()
        self.state = .empty

        movieSource.fetchMostPopular(genres: selectedGenresStringIds) { [weak self]result in
            
            guard let `self` = self else {
                return
            }
            
            if let value = result.value {
                
                self.mostPopularMovies = value.map {
                    
                    FeaturedMovieStub(
                        movieId: $0.movieId,
                        genres: GenreManager.shared.map(ids: $0.genreIds!).map { $0.name },
                        posterPath: $0.posterPath ?? "",
                        originalTitle: $0.originalTitle,
                        releaseDate: nil,
                        imagePath: $0.imagePath ?? "",
                        overview: $0.overview ?? "",
                        rating: $0.voteAverage ?? 0,
                        character: "")
                }

                self.delegate?.dataDownloaded()
                
                self.state = self.mostPopularMovies.isEmpty ? .empty : .ready
            } else {
                
                if self.state == .error {
                    return
                }
                
                guard let error = result.error else {
                    self.delegate?.handleError(MoovieError.genericError)
                    return
                }
                
                self.error = result.error
                self.state = .error
                self.delegate?.handleError(error)
            }
        }
    }
    
    func getUserFavoriteMovies() {
        
        guard let sessionID = keychain.sessionID else {
            
            error = MoovieError.missingSessionId
            state = .error
            delegate?.handleError(MoovieError.missingSessionId)
            return
        }
        
        guard let accountID = keychain.accountID else {
            
            error = MoovieError.missingAccountId
            state = .error
            delegate?.handleError(MoovieError.missingAccountId)
            return
        }
        
        userDataSource.fetchUserfavoriteMovies(sessionID: sessionID, accountID: accountID ) {  [weak self] result in
            
            guard let `self` = self else {
                return
            }
            
            if let value = result.value {
                
                var movieIDs = [Int]()
                for id in value {
                    movieIDs.append(id.movieId)
                }
                self.userDefaultsStorage.favoriteMovies = movieIDs
                
            } else {
                
                guard let error = result.error else {
                    self.delegate?.handleError(MoovieError.genericError)
                    return
                }
                
                self.error = result.error
                self.state = .error
                self.delegate?.handleError(error)
            }
        }
    }
}
