//
//  SearchViewModel.swift
//  Moovie
//
//  Created by Michal Sverak on 8/15/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//

import Foundation
import Alamofire

protocol SearchViewModelDelegate: class {
    func updateView()
    func handleError(_ error: Error)
}

protocol Item {}

class SearchViewModel {
    
    var actors = [PopularActor]()
    var movies = [FeaturedMovieStub]()
    weak var delegate: SearchViewModelDelegate?
    var error: Error?
    var state: ViewModelState?
    let movieSource: MovieSource
    let actorSource = AlamofireActorSource()
    var userDefaultsStorage: UserDefaultsStorage
    
    //MARK: ViewModel init + Dependency injection
    
    static func resolve() -> SearchViewModel? {
        
        return DependencyInjectionContainer.resolve(SearchViewModel.self)
    }
    
    init(movieSource: MovieSource, userDefaultsStorage: UserDefaultsStorage) {
        
        self.movieSource = movieSource
        self.userDefaultsStorage = userDefaultsStorage
    }
    
    //MARK: ViewModel methods
    
    func searchForMovies(queryString: String) {
        
        guard !queryString.isEmpty else {
            return
        }
        
        if state == .loading {
            stopAllSessions()
        }
        
        self.state = .loading
    
        self.movieSource.searchForMovies(queryString: queryString) { [weak self] result in
            
            guard let `self` = self else {
                return
            }
            
            if let value = result.value {
                
                self.movies = value.map {
                    
                    FeaturedMovieStub(
                        movieId: $0.movieId,
                        genres: GenreManager.shared.map(ids: $0.genreIds!).map { $0.name },
                        posterPath: $0.posterPath ?? "",
                        originalTitle: $0.originalTitle,
                        releaseDate: $0.releaseDate?.yearToString(),
                        imagePath: $0.imagePath ?? "",
                        overview: $0.overview ?? "",
                        rating: $0.voteAverage ?? 0,
                        character: "")
                }
                
                self.delegate?.updateView()
                self.state = self.movies.isEmpty ? .empty : .ready
             
            } else {
                if let error = result.error {
                    let errorCode = (error as NSError).code
                    
                    // Do not call delegeate to handle error if error is request canceled error (code: -999)
                    guard errorCode != -999 else {
                        return
                    }
                    
                    self.error = error
                    self.state = .error
                    self.delegate?.handleError(error)
                }
            }
        }
    }
    
    func searchForActors(queryString: String) {
        
        guard !queryString.isEmpty else {
            return
        }
        
        if state == .loading {
            stopAllSessions()
        }
        
        self.state = .loading
        
        actorSource.searchForActor(queryString: queryString) { [weak self] result in
            
            guard let `self` = self else {
                return
            }
            
            if let value = result.value {
                self.actors = value
                self.delegate?.updateView()
                self.state = self.actors.isEmpty ? .empty : .ready
            } else {

                // Do not call delegeate to handle error if error is request canceled error (code: -999)
                if let error = result.error {
                    let errorCode = (error as NSError).code
                    print(errorCode)
                    
                    guard errorCode != -999 else {
                        return
                    }
                    
                    self.error = error
                    self.state = .error
                    self.delegate?.handleError(error)
                }
            }
        }
    }
    
    func stopAllSessions() {
        
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach { $0.cancel() }
        }
    }
}
