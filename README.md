# MOOVIES README #

### Description ###

	Moovie app is a movie database app based on The Movie Database API (version 3.0).
	For more info about the API go to: https://developers.themoviedb.org/3

### Requirements ###

	iOS 10+
	Xcode 8.3+
	Swift 3.1+

### Setup ###

	version 1.0

	Moovie app is using Cocoa pods. Pods are not included in the repository.
	In order to install pods run following command in project folder: $ pod install

### Testing ###

	For all testing you can use following MovieDB account:

	username: majkitos 
	password: majkitos

### Who do I talk to? ###
	
	Admin: Michal Svěrák
	contact: m.sverak@gmail.com