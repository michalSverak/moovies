//
//  MovieDetailViewModelTests.swift
//  Moovie
//
//  Created by Michal Sverak on 9/15/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//
// Source: Jan Kaltoun @STRV

import Quick
import Nimble
import KeychainSwift
@testable import Moovie

class MovieDetailViewModelTests: QuickSpec {
    
    class MockMovieSource: MovieSource {
        
        var movie: Movie?
        var error: Error?
        
        init(movie: Movie? = nil, error: Error? = nil) {
            
            self.movie = movie
            self.error = error
        }
        
        func fetchMovieDetail(id: String, completion: @escaping (APIResult<Movie>) -> Void) {
            
            if let error = error {
                
                completion(APIResult.failure(error))
                
                return
            }
            
            guard let movie = movie else {
                
                fail("Missing movie object")
                
                return
            }
            
            completion(APIResult.success(movie))
        }
        
        func fetchNowPlaying(genres: String, completion: @escaping (APIResult<[FeaturedMovie]>) -> Void) {
            
            fatalError("Not implemented for this test.")
        }
        
        func fetchMostPopular(genres: String, completion: @escaping (APIResult<[FeaturedMovie]>) -> Void) {
            
            fatalError("Not implemented for this test.")
        }
        
        func markMovieAsFavorite(movieID: Int, sessionID: String, accountID: String, favorite: Bool, completion: @escaping (APIResult<PostedMovieStatus>)->Void) {
            
             fatalError("Not implemented for this test.")
        }
        func searchForMovies(queryString: String, completion: @escaping (APIResult<[FeaturedMovie]>) -> Void) {
            
             fatalError("Not implemented for this test.")
        }
    }
    
    class MockMovieDetailViewModelDelegate: MovieDetailViewModelDelegate {
        
        var expectations: () -> Void
        
        init(expectations: @escaping () -> Void) {
            
            self.expectations = expectations
        }
        
        func dataUpdated() {
            
            expectations()
        }
        
        func handleError(_ error: Error) {
            
            fatalError("Not implemented for this test.")
        }
        func updateFavorite() {
            
            fatalError("Not implemented for this test.")
        }
    }
    
    struct Constants {
        
        static let movie = Movie(
            movieId: 123,
            originalTitle: "I have no idea",
            adult: true,
            overview: "sdfsdfsfd",
            releaseDate: "sdfsdfsdsdf",
            hasVideo: true,
            actors: [],
            crew: []
        )
    }
    
    override func spec() {
        
        describe("Testing Movie Detail View Model") {
            
            it("Fetches movie detail correctly") {
                
                let service = MockMovieSource(movie: Constants.movie)
                let keychain = MoovieKeychainManager(keychain: KeychainSwift())
                let viewModel = MovieDetailViewModel(movieSource: service, keychain: keychain, userDefaultsStorage: MoovieUserDefaultsStorage())
                viewModel.movieId = 123
                
                var numberOfPasses = 0
                
                var finishedWithSuccess = false
                
                let delegate = MockMovieDetailViewModelDelegate(expectations: {
                    
                    numberOfPasses += 1
                    
                    if numberOfPasses == 3 {
                        
                        expect(viewModel.movieDetail?.movieId).to(equal(Constants.movie.movieId))
                        
                        finishedWithSuccess = true
                    }
                })
                
                viewModel.delegate = delegate
                
                viewModel.getData()
                
                expect(finishedWithSuccess).toEventually(beTrue())
            }
        }
    }
}

