//
//  MoovieTests.swift
//  MoovieTests
//
//  Created by Michal Sverak on 9/15/17.
//  Copyright © 2017 MichalSverak. All rights reserved.
//
//           _.-'~~~~~~`-._
//          /      ||      \
//         /       ||       \
//        |        ||        |
//        | _______||_______ |
//        |/ ----- \/ ----- \|
//       /  (     )  (     )  \
//      / \  ----- () -----  / \
//     /   \      /||\      /   \
//    /     \    /||||\    /     \
//   /       \  /||||||\  /       \
//  /_        \o========o/        _\
//    `--...__|`-._  _.-'|__...--'
//            |    `'    |
//
// I FIND YOUR LACK OF UNIT TESTS DISTURBING

import Quick
import Nimble
@testable import Moovie

class MovieSourceTests: QuickSpec {
    
    struct Constants {
        
        static let movie = Movie(
            movieId: 123,
            originalTitle: "I have no idea",
            adult: true,
            overview: "blah",
            releaseDate: "sdfsdfsdsdf",
            hasVideo: true,
            actors: [],
            crew: []
        )
    }
    
    override func spec() {
        
        describe("Testing Movie Source") {
            
            it("Fetches movie detail correctly") {
                
                let apiManager = MockAPIManager()
                let service = AlamofireMovieSource(apiManager: apiManager)
                
                var finishedWithSuccess = false
                
                service.fetchMovieDetail(id: String(Constants.movie.movieId)) { result in
                    
                    expect(result.value).to(equal(Constants.movie))
                    
                    finishedWithSuccess = (result.error == nil)
                }
                
                expect(finishedWithSuccess).toEventually(beTrue())
            }
        }
    }
}
